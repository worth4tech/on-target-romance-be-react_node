openssl genrsa -out server.key 2048
openssl req -new -x509 -key server.key -out server.cert -days 3650 -subj /CN=ec2-52-11-154-108.us-west-2.compute.amazonaws.com