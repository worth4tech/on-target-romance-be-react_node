ALTER TABLE `one-target-romance`.`users` 
CHANGE COLUMN `fname` `fname` VARCHAR(100) NULL DEFAULT NULL ,
CHANGE COLUMN `lname` `lname` VARCHAR(100) NULL DEFAULT NULL ;


ALTER TABLE `one-target-romance`.`users` 
DROP FOREIGN KEY `fk_users_country`,
DROP FOREIGN KEY `fk_users_type`;
ALTER TABLE `one-target-romance`.`users` 
ADD CONSTRAINT `fk_users_country`
  FOREIGN KEY (`country_id`)
  REFERENCES `one-target-romance`.`county_master` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_users_type`
  FOREIGN KEY (`type_id`)
  REFERENCES `one-target-romance`.`user_type` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;




  ALTER TABLE `one-target-romance`.`events` 
DROP FOREIGN KEY `fk_user_id_users`;
ALTER TABLE `one-target-romance`.`events` 
ADD CONSTRAINT `fk_user_id_users`
  FOREIGN KEY (`user_id`)
  REFERENCES `one-target-romance`.`users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;



  ALTER TABLE `one-target-romance`.`follow` 
DROP FOREIGN KEY `follow_following_user_id_users`,
DROP FOREIGN KEY `follow_user_id_users`;
ALTER TABLE `one-target-romance`.`follow` 
ADD CONSTRAINT `follow_following_user_id_users`
  FOREIGN KEY (`following_user_id`)
  REFERENCES `one-target-romance`.`users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `follow_user_id_users`
  FOREIGN KEY (`user_id`)
  REFERENCES `one-target-romance`.`users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;




 ALTER TABLE `one-target-romance`.`transaction_logs` 
DROP FOREIGN KEY `fk_transaction_logs_1`,
DROP FOREIGN KEY `fk_transaction_logs_2`;
ALTER TABLE `one-target-romance`.`transaction_logs` 
ADD CONSTRAINT `fk_transaction_logs_1`
  FOREIGN KEY (`membership_id`)
  REFERENCES `one-target-romance`.`membership_master` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_transaction_logs_2`
  FOREIGN KEY (`user_id`)
  REFERENCES `one-target-romance`.`users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;