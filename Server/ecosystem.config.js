module.exports = {
  apps: [{
    autorestart: true,
    name: 'app',
    script: 'app.js',
    log: `./logs/${new Date().toISOString().slice(0,10)}.log`,
    instances : "max",
    exec_mode : "cluster"
  }]
}