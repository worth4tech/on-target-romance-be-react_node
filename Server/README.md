# SparkXIO parking solution software
SpaekXIO

## TODO: APP logo.

## Development

#### Prerequisites:-
* Node.js v8.0.0
* NPM v5.6.0
* mysql-server 5.7.*

#### How to run ?
* Clone the repo.
* Create `.env` at root.(Take the reference from example.env).
* Create database.
* Run `migrate.sh` script to run migrations(Make sure sql-server is running).
```
./migrate.sh
```
* Install dependences by
```
npm i
```
* Run the app(For development) by
```
npm run-script start-dev`
```
* Run the app by(For production)
```
npm start
```
* Server should srart on port 3000. (Can be change by by `PORT` environment variable or change in `.env`)

#### TODO: How to run test cases ?

#### TODO: API documentation.