const nodemailer = require('nodemailer');
const mailerhbs = require('nodemailer-express-handlebars');
const path = require('path');

// Nodemailer configuration
const transporter = nodemailer.createTransport({
  service: process.env.GMAIL_SERVICE_NAME,
  host: process.env.GMAIL_SERVICE_HOST,
  secure: process.env.GMAIL_SERVICE_SECURE,
  port: process.env.GMAIL_SERVICE_PORT,
  auth: {
    user: process.env.GMAIL_USER_NAME,
    pass: process.env.GMAIL_USER_PASSWORD,
  },
});

// Handlebar configuration with nodemailer
transporter.use('compile', mailerhbs({
  viewEngine: {
    extname: '.hbs',
    layoutsDir: path.join(__dirname, '/../../', 'views/email_templates/layouts'),
    defaultLayout: 'layout',
    partialsDir: 'views/',
  },
  viewPath: path.join(__dirname, '/../../', 'views/email_templates/'),
  extName: '.hbs',
}));

module.exports.sendEmail = (from, to, subject, template, context) => new Promise((resolve, reject) => {
  transporter.sendMail(
    {
      from,
      to,
      subject,
      template,
      context,
    }, (error, response) => {
      // Email not sent
      if (error) {
        console.log('Error while sending an email. Error is:- ', error);
        return reject(error);
      }
      return resolve(response);
    }
  );
});
