const jwt = require('jsonwebtoken');

module.exports.generateToken = data => jwt.sign(data, process.env.JWT_SECRET, { expiresIn: process.env.JWT_EXPIRY });

module.exports.generateOTP = () => Math.floor(100000 + Math.random() * 900000);

module.exports.randomString = (length = 32) => {
  const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let result = '';
  for (let i = length; i > 0; i -= 1) result += chars[Math.round(Math.random() * (chars.length - 1))];
  return result;
};
