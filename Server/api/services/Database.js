const _ = require('lodash');
const assert = require('assert');
const Promise = require('bluebird');

const Pooling = require('generic-pool');
const squel = require('squel').useFlavour('mysql');
const mysql = require('mysql');

// Set squel options
squel.cls.DefaultQueryBuilderOptions.autoQuoteFieldNames = true;
squel.cls.DefaultQueryBuilderOptions.autoQuoteTableNames = true;
squel.cls.DefaultQueryBuilderOptions.autoQuoteAliasNames = true;
squel.cls.DefaultQueryBuilderOptions.nameQuoteCharacter = '';
squel.cls.DefaultQueryBuilderOptions.tableAliasQuoteCharacter = '';
squel.cls.DefaultQueryBuilderOptions.fieldAliasQuoteCharacter = '';

// Database Connection
const masterConfig = {
  host: process.env.DB_HOST || '127.0.0.1',
  user: process.env.DB_USER || 'root', // env var: PGUSER
  database: process.env.DB_NAME || 'one-target-romance', // env var: PGDATABASE
  password: process.env.DB_PASS || 'root', // env var: PGPASSWORD
  max: process.env.DB_MAX_CLIENTS || 10, // max number of clients in the pool
  min: process.env.DB_MIN_CLIENTS || 2, // set min pool size to 4
  idleTimeoutMillis: process.env.DB_TIMEOUT || 10000, // how long a client is allowed to remain idle before being closed
};

let reads = 0;
function getConnectionString({
  user, password, host, database,
}) {
  return `mysql://${user}:${password}@${host}/${database}`;
}

function generatePgConnectionString(config, replicaHosts) {
  const connections = [];
  connections.push(getConnectionString(config));

  if (replicaHosts) {
    replicaHosts.forEach((replicaHost) => {
      const hostPort = replicaHost.split(':');
      connections.push(getConnectionString(Object.assign({}, config, {
        host: hostPort[0],
      })));
    });
  }

  return connections;
}

const replicaConnections = generatePgConnectionString(masterConfig, process.env.POSTGRES_READ_REPLICA_CONNECTIONS && process.env.POSTGRES_READ_REPLICA_CONNECTIONS.split(','));

const pool = {
  release: (client) => {
    if (client.queryType === 'read') {
      return pool.read.release(client);
    }
    return pool.write.release(client);
  },
  acquire: ({ priority, queryType, useMaster }) => {
    useMaster = _.isUndefined(useMaster) ? false : useMaster;
    if (queryType === 'read' && !useMaster) {
      return pool.read.acquire(priority);
    }
    return pool.write.acquire(priority);
  },
  destroy: (connection) => {
    console.log('connection destroy');
    return pool[connection.queryType].destroy(connection);
  },
  clear: () => {
    console.log('all connection clear');
    return Promise.join(
      pool.read.clear(),
      pool.write.clear()
    );
  },
  drain: () => Promise.join(
    pool.write.drain(),
    pool.read.drain()
  ),
  read: Pooling.createPool({
    create: () => {
      reads += 1;
      const nextRead = reads % replicaConnections.length; // round robin config

      return new Promise((resolve, reject) => {
        const client = mysql.createConnection(replicaConnections[nextRead]);

        client.connect((err) => {
          if (err) {
            reject(err);
          } else {
            resolve(client);
          }
        });
      })
        .then((connection) => {
          connection.queryType = 'read';
          return connection;
        });
    },
    destroy: connection => new Promise((resolve) => {
      connection.end();
      resolve();
    }),
    validate: connection => connection._invalid === undefined,
  }, {
    Promise,
    max: process.env.DB_MAX_CLIENTS,
    min: process.env.DB_MIN_CLIENTS,
    testOnBorrow: true,
    autostart: true,
    idleTimeoutMillis: process.env.DB_TIMEOUT,
    evictionRunIntervalMillis: 3600000,
    acquireTimeoutMillis: process.env.DB_TIMEOUT,
  }),
  write: Pooling.createPool({
    create: () => new Promise((resolve, reject) => {
      const client = mysql.createConnection(masterConfig);

      client.connect((err) => {
        if (err) {
          reject(err);
        } else {
          resolve(client);
        }
      });
    })
      .then((connection) => {
        connection.queryType = 'write';
        return connection;
      }),
    destroy: connection => new Promise((resolve) => {
      connection.end();
      resolve();
    }),
    validate: connection => connection._invalid === undefined,
  }, {
    Promise,
    max: process.env.DB_MAX_CLIENTS,
    min: process.env.DB_MIN_CLIENTS,
    testOnBorrow: true,
    autostart: true,
    idleTimeoutMillis: process.env.DB_TIMEOUT,
    evictionRunIntervalMillis: 3600000,
    acquireTimeoutMillis: process.env.DB_TIMEOUT,
  }),
};

// Export
module.exports = {
  executeQuery: function executeQuery(query, values) {
    let queryType;
    if (query.text) {
      queryType = _.startsWith(query.text, 'SELECT') ? 'read' : 'other';
    } else {
      queryType = _.startsWith(query, 'SELECT') ? 'read' : 'other';
    }

    return pool.acquire({ queryType })
      .then(connection => new Promise((resolve, reject) => {
        if (query.text) {
          return connection.query(query.text, query.values, (err, result) => {
            if (err) {
              reject(err);
            } else {
              resolve(result);
            }
          });
        }
        return connection.query(query, values, (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      })
        .then(result => Promise.all([
          result,
          pool.release(connection),
        ]), error => pool.release(connection)
          .then(() => {
            // Also important to escalate the error to handlers
            throw error;
          })))
      .then(([result]) => result);
  },
  builder () {
    return squel;
  },

  // drain connection.
  drainConnection() {
    return pool.drain()
      .then(() => ({ drainStatus: true }))
      .catch(() => {
        throw new Error();
      });
  },

  executeTransaction(queries) {
    try {
      assert.ok(queries && queries.length, 'Queries must be in array');
    } catch (error) {
      throw error;
    }

    return pool.acquire({ queryType: 'other' })
      .then((connection) => {
        Promise.promisifyAll(connection);

        return connection.beginTransactionAsync()
          .then(() => Promise.all(queries.map(query => connection.queryAsync(query.text, query.values))))
          .then(response => connection.commitAsync()
            .then(response))
          .then(result => Promise.all([
            result,
            pool.release(connection),
          ]), error => connection.rollbackAsync()
            .then(() => pool.release(connection)
              .then(() => {
                throw error;
              })))
          .then(([result]) => result);
      });
  },


  getTransactionConnection() {
    return pool.acquire({ queryType: 'other' })
      .then((connection) => {
        Promise.promisifyAll(connection);

        return connection.beginTransactionAsync()
          .then(() => connection);
      });
  },

  executeTransactionQuery(connection, query, values) {
    return new Promise((resolve, reject) => {
      if (query.text) {
        return connection.query(query.text, query.values, (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      }
      return connection.query(query, values, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    })
      .then(result => Promise.all([
        result,
      ]), error => connection.rollbackAsync()
        .then(() => pool.release(connection)
          .then(() => {
            throw error;
          })));
  },

  commitTransaction(connection) {
    Promise.promisifyAll(connection);

    return connection.commitAsync()
      .then(result => Promise.all([
        result,
        pool.release(connection),
      ]), error => connection.rollbackAsync()
        .then(() => pool.release(connection)
          .then(() => {
            throw error;
          })))
      .then(([result]) => result);
  },
};
