const bcrypt = require('bcrypt');

const saltRounds = 10;

module.exports.generatePasswordHash = password => new Promise((resolve, reject) => {
  bcrypt.hash(password, saltRounds, (error, res) => {
    if (error) {
      return reject(error);
    }
    return resolve(res);
  });
});

module.exports.checkPassword = (plainPassword, encryptedPasswod) => new Promise((resolve, reject) => {
  bcrypt.compare(plainPassword, encryptedPasswod, (error, res) => {
    if (error) {
      return reject(error);
    }
    return resolve(res);
  });
});
