const Joi = require('joi');

module.exports.addNewFollowingUserWhileSignup = (req, res, next) => {
    const schema = Joi.object().keys({
        following_user_id: Joi.number().required(),
        user_id: Joi.number().required()
    });
    
    Joi.validate(req.body, schema, {
        convert: true,
    }, (err) => {
        if (err) {
            return res.status(400).json({
                message: err.details[0].message,
            });
        }
        next();
    });
};

module.exports.addNewFollowingUser = (req, res, next) => {
    const schema = Joi.object().keys({
        following_user_id: Joi.number().required()
    });

    Joi.validate(req.body, schema, {
        convert: true,
    }, (err) => {
        if (err) {
            return res.status(400).json({
                message: err.details[0].message,
            });
        }
        next();
    });
};