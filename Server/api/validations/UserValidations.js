const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');

const Joi = BaseJoi.extend(Extension);

module.exports.signupStep1 = (req, res, next) => {
  const schema = Joi.object().keys({
    type_id: Joi.number().min(1).required(),
    display_name: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(6).required(),
    country_id: Joi.number().min(1).required()
  });

  Joi.validate(req.body, schema, {
    convert: true,
  }, (err) => {
    if (err) {
      return res.status(400).json({
        message: err.details[0].message,
      });
    }
    next();
  });
};

module.exports.resendOtp = (req, res, next) => {
  const schema = Joi.object().keys({
    email: Joi.string().email().required(),
  });

  Joi.validate(req.body, schema, {
    convert: true,
  }, (err) => {
    if (err) {
      return res.status(400).json({
        message: err.details[0].message,
      });
    }
    next();
  });
};

module.exports.verifyEmail = (req, res, next) => {
  const schema = Joi.object().keys({
    email: Joi.string().email().required(),
    otp: Joi.number().min(6).required()
  });

  Joi.validate(req.body, schema, {
    convert: true,
  }, (err) => {
    if (err) {
      return res.status(400).json({
        message: err.details[0].message,
      });
    }
    next();
  });
};

module.exports.login = (req, res, next) => {
  const schema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  });
  Joi.validate(req.body, schema, {
    convert: true,
  }, (err) => {
    if (err) {
      return res.status(400).json({
        message: err.details[0].message
      });
    }
    next();
  });
};

module.exports.forgotPassword = (req, res, next) => {
  const schema = Joi.object().keys({
    email: Joi.string().email().required()
  });
  
  Joi.validate(req.body, schema, {
    convert: true,
  }, (err) => {
    if (err) {
      return res.status(400).json({
        message: err.details[0].message
      });
    }
    next();
  });
};

module.exports.resetPassword = (req, res, next) => {
  const schema = Joi.object().keys({
    forgot_password_code: Joi.string().required(),
    password: Joi.string().min(6).required(),
    confirmPassword: Joi.string().valid(Joi.ref('password')).min(6).required().options({ language: { any: { allowOnly: 'must match with password' } } })
  });
  
  Joi.validate(req.body, schema, {
    convert: true,
  }, (err) => {
    if (err) {
      return res.status(400).json({
        message: err.details[0].message
      });
    }
    next();
  });
};

module.exports.profileSetup = (body) => {
  return new Promise((resolve, reject) => {
    const schema = Joi.object().keys({
      type_id: Joi.number().required(),
      personal_details: Joi.object().keys({
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        middleName: Joi.string()
      }),
      contact_details: Joi.object().keys({
        email: Joi.string().email(),
        is_email_public: Joi.boolean()
      }),
      social_media_details: Joi.object().keys({
        goodreads_link: Joi.string().uri().trim().required(),
        website: Joi.string().uri().trim(),
        facebook_profile_link: Joi.string().uri().trim(),
        reader_group: Joi.object().keys({
          name: Joi.string().trim(),
          type: Joi.string().trim(),
          links: Joi.array().items(Joi.string().uri().trim())
        }),
        available_for_purchase: Joi.array().items(Joi.string().trim())
      }),
      bio: Joi.string().trim()
    });
    
    Joi.validate(body, schema, {
      convert: true,
    }, (err) => {
      if (err) {
        return reject({
          status: 400,
          message: err.details[0].message
        });
      }
      return resolve();
    });
  });
};