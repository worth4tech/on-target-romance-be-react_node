const jwt = require('jsonwebtoken');
const CommonQuery = require('../models/commonQueryModel');

module.exports = function(req, res, next) {
  const token = req.headers['x-authorization'];
  // Check token is exists into header or not
  if (token) {
    // Token is exists but let's check this token is valid or not
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (err || !decoded.id) {
        // Invalid token.
        return res.status(403).json({
          message: 'Invalid token',
        });
      }

      // Token is valid but let's check this user is actull exists into DB or not.
      CommonQuery.selectRecord(
        ['*'],
        [
          {
            key: 'id',
            value: decoded.id,
          },
        ],
        'users'
      )
        .then((response) => {
          if (response && response.length) {
            // This user is exists into the DB. So we need to move into next step.
            req.id = decoded.id;
            next();
          } else {
            // This user is not exists into DB.
            return res.status(403).json({
              message: 'Invalid user',
            });
          }
        })
        .catch(error => res.status(403).json({
          message: error.message || 'Something went wrong. Please try again.',
        }));
    });
  } else {
    // Token is not exists into the request
    return res.status(403).json({
      message: 'Invalid token',
    });
  }
};
