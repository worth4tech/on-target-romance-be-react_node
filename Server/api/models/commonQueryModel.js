const Database = require('../services/Database.js');

module.exports.addNewRecord = (data, tableName) => {
    const addNewRequest = Database.builder()
        .insert()
        .into(tableName)
        .setFieldsRows(data)
        .toParam();

    return Database.executeQuery(addNewRequest)
        .then((result) => result)
        .catch((e) => {
            throw e;
        });
};

module.exports.deleteRecord = (conditions, tableName) => {
    let query = Database.builder()
        .delete()
        .from(tableName);

    for (let i = 0; i < conditions.length; i++) {
        query = query
            .where(`${conditions[i].key} = ?`, conditions[i].value)
    }

    query = query.toParam();
    return Database.executeQuery(query);
};

module.exports.updateRecord = (updateData, conditions, tableName) => {
    let query = Database.builder()
        .update()
        .table(`${tableName}`)
        .setFields(updateData);

    for (let i = 0; i < conditions.length; i++) {
        query = query
            .where(`${conditions[i].key} = ?`, conditions[i].value)
    }

    query = query.toParam();
    return Database.executeQuery(query);
};

module.exports.selectRecord = (selectColumn, conditions, tableName, sortBy = undefined) => {
    let query = Database.builder()
        .select();

    for (let i = 0; i < selectColumn.length; i++) {
        query = query
            .field(selectColumn[i])
    }

    for (let i = 0; i < conditions.length; i++) {
        if (conditions[i].key && conditions[i].value) {
            query = query
                .where(`${conditions[i].key} = ?`, conditions[i].value);
        } else if (conditions[i].key && !conditions[i].conditions) {
            query = query
                .where(`${conditions[i].key}`);
        }
    }

    if (sortBy) {
        query = query.order(sortBy, true)
    }

    query = query.from(tableName).toParam();
    console.log("query -===?>", query);
    return Database.executeQuery(query);
};