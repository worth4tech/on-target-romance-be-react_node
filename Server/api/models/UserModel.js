const Database = require('../services/Database.js');

module.exports.fetchUserDetailsWithMemberShip = (userId) => {
    const query = Database.builder()
        .select()
        .field('m.cost')
        .field('m.id')
        .field('m.validity')
        .field('u.type_id')
        .left_join('membership_master', 'm', 'm.user_type_id = u.type_id')
        .from('users', 'u')
        .where('u.id = ?', userId)
        .toParam();

    return Database.executeQuery(query);
};

module.exports.fetchUserTypeWithDescription = () => {
    const query = Database.builder()
        .select()
        .field('ut.type_name')
        .field('ut.id')
        .field('m.description')
        .field('m.cost')
        .field('m.validity')
        .left_join('membership_master', 'm', 'm.user_type_id = ut.id')
        .from('user_type', 'ut')
        .toParam();
    
    return Database.executeQuery(query);
};

module.exports.fetchFullUserProfile = (userId) => {
    const query = Database.builder()
        .select()
        .field('ut.type_name')
        .field('u.*')
        .left_join('user_type', 'ut', 'ut.id = u.type_id')
        .from('users', 'u')
        .where('u.id = ?', userId)
        .toParam();

    return Database.executeQuery(query);
};

module.exports.getUserSchema = (userType) => {

    const query = Database.builder()
        .select()
        .field('form_schema')
        .from('form_schema')
        .where('form_schema_name = ?', userType)
        .toParam();

    return Database.executeQuery(query)
    .then((result) => {
        if (result.length && result[0].form_schema) {
            return JSON.parse(result[0].form_schema);
        } else {
            return [];
        }
    })
    .catch((e) => {
        throw e;
    });
};

module.exports.getAllUsers = (limit, skip, userType, search) => {
    let query = Database.builder()
        .select()
        .field('*')
        .from('users');
    
    if (userType > 0) {
        query = query
            .where('type_id = ?', userType);
    }

    if (search) {
        query = query
            .where('display_name LIKE ? or email LIKE ?', `%${search}%`, `%${search}%`);
    }

    query = query
        .order('id', false)
        .group('id')
        .limit(limit)
        .offset(skip)
        .toParam();
    console.log("query ", query);
    
    return Database.executeQuery(query);
};