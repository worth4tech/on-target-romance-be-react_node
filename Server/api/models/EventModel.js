const Database = require('../services/Database.js');
const _ = require('lodash');
const moment = require('moment');

module.exports.getEventsByUserById = (userId) => {
    const query = Database.builder()
        .select()
        .field('id')
        // .field('event_details')
        .field('created_date')
        .field('updated_date')
        .field('event_type')
        .field('start_date as start')
        .field('end_date as end')
        .field('author_first_name')
        .field('author_last_name')
        .field('title')
        .field('hosted_by')
        .field('event_details_with_values')
        .from('events')
        .where('user_id = ?', userId)
        .toParam();
        
        return Database.executeQuery(query);
};
module.exports.getEventsByUserByType = (userType) => {
    
    if(userType ==='Reader' || userType ==='Vendor')
    {
        const event = 'event_type_3';
        const query = Database.builder()
        .select()
        .field('id')
        // .field('event_details')
        .field('created_date')
        .field('updated_date')
        .field('event_type')
        .field('start_date as start')
        .field('end_date as end')
        .field('author_first_name')
        .field('author_last_name')
        .field('title')
        .field('hosted_by')
        .field('event_details_with_values')
        .from('events')
        .where('event_type <> ?',event)
        .toParam();

        return Database.executeQuery(query);
    }
    else if(userType ==='Author'||userType ==='Blogger'||userType ==='Blogger Premium'||userType ==='PR Firm')
    {
        const query = Database.builder()
        .select()
        .field('id')
        // .field('event_details')
        .field('created_date')
        .field('updated_date')
        .field('event_type')
        .field('start_date as start')
        .field('end_date as end')
        .field('author_first_name')
        .field('author_last_name')
        .field('title')
        .field('hosted_by')
        .field('event_details_with_values')
        .from('events')
        .toParam();

        return Database.executeQuery(query);
    }
    /*const query = Database.builder()
        .select()
        .field('*')
        // .field('event_details')
        .field('created_date')
        .field('updated_date')
        .field('event_type')
        .field('start_date as start')
        .field('end_date as end')
        .field('author_first_name')
        .field('author_last_name')
        .field('title')
        .field('hosted_by')
        .field('event_details_with_values')
        .from('events');
        
        return Database.executeQuery(query);*/
};

module.exports.getKeyValueObjectFromMetaJson = (mainJson,eventType) => {
    return new Promise((resolve, reject) => {
        try {
            
            let newConvertedJson = {
                convertedJSONs: [],
                start: moment.utc().format('YYYY-MM-DD'),
                end: undefined,
                title: '',
            };

            mainJson = _.orderBy(mainJson, ['index'], ['asc']);

            for(let i = 0; i < mainJson.length; i++) {
                let convertedJSON = {};
                const singleJson = mainJson[i];
                if (singleJson.hasOwnProperty('index')) {
                    if (singleJson.tag === 'custom') {
                        console.log("Getting custom component ", singleJson);
                        
                        // Removing services objects from children array. We don't required that objed details into new object
                        const withoutService = singleJson.children.filter((key) => key.type !== "service");
                        console.log("withoutService", withoutService);
                        
                        // First we need to check we have any childrenObj key or not
                        if (singleJson && singleJson.childrenobj) {
                            // Create blank array based on this custom title.
                            convertedJSON[singleJson.title] = [];
        
                            
                            // Loop through service count. This indicates total how much time user added new list with add new functionality
                            for (let k = 1; k <= parseInt(singleJson.service); k++) {
                                let cusotmObj = [];
        
                                for (let j = (( k - 1) * singleJson.childrenobj.length); j < (k * singleJson.childrenobj.length); j++) {
                                    let grandChild = {};
                                    
                                    console.log(withoutService[j]['name'], ":", withoutService[j]['value']);

                                    if (withoutService[j]['value'] && withoutService[j]['value'] !== "") {
                                        if (withoutService[j]['display_name']) {
                                            grandChild[withoutService[j]['display_name']] = withoutService[j].value;
                                        } else {
                                            grandChild[withoutService[j]['name']] = withoutService[j].value;
                                        }
                                        cusotmObj.push(grandChild);
                                    } else continue
                                }
                                
                                if (cusotmObj && cusotmObj.length) {
                                    convertedJSON[singleJson.title].push(cusotmObj);
                                }
                            }
                        } else {
                            // Create blank object based on this custom title.
                            let grandChild = [];
                            convertedJSON[singleJson.title] = [];
                            
                            // Loop through service count. This indicates total how much time user added new list with add new functionality
                            for (let j = 0; j < withoutService.length; j++) {
                                if (withoutService[j]['value'] && withoutService[j]['value'] !== "") {
                                    if (withoutService[j]['display_name']) {
                                        grandChild.push(
                                            {
                                                [withoutService[j]['display_name']]: withoutService[j].value
                                            }
                                        );
                                    } else {
                                        grandChild.push(
                                            {
                                                [withoutService[j]['name']]: withoutService[j].value
                                            }
                                        );
                                    }
                                } else continue
                            }
                            if (grandChild && grandChild.length) {
                                convertedJSON[singleJson.title].push(grandChild);
                            }
                        }
                    } else if (singleJson.tag === 'title') {
                        if (singleJson.display_name) {
                            convertedJSON[singleJson.display_name] = mainJson[i].value;
                        } else {
                            convertedJSON[singleJson.title] = mainJson[i].value;
                        }
                    
                    } else if(singleJson.name === 'start_date') {
                        newConvertedJson['org_start'] = moment.utc(singleJson.value).format('YYYY-MM-DD HH:mm:ss');;
                        convertedJSON[singleJson.display_name] = moment.utc(singleJson.value).format('MMMM, DD YYYY');
                    } else if(singleJson.name === 'end_date') {
                        newConvertedJson['org_end'] = moment.utc(singleJson.value).format('YYYY-MM-DD');
                        convertedJSON[singleJson.display_name] = moment.utc(singleJson.value).format('MMMM, DD YYYY');
                    }  else if (singleJson.tag === 'date') {
                        if (singleJson.value && singleJson.value.length) {
                            const formattedDate = moment.utc(singleJson.value).format('MMMM, DD YYYY');
                            if (singleJson.display_name) {
                                convertedJSON[singleJson.display_name] = formattedDate;
                            } else {
                                convertedJSON[singleJson.name] = formattedDate;
                            }
                        }
                    } else if(singleJson.name === 'title') {
                        newConvertedJson['title'] = singleJson.value;
                        convertedJSON[singleJson.display_name] = singleJson.value;
                    } else if(singleJson.tag==='radio' && eventType==='event_type_2')
                    {
                        if(singleJson.value==='Yes')
                        {
                            convertedJSON[singleJson.display_name]= singleJson.value;
                        }
                    } else if(singleJson.tag==='radio' && eventType==='event_type_4') {
                        if(singleJson.value==='Yes') {
                            convertedJSON[singleJson.display_name]= singleJson.value;
                        }
                    } else {
                        if (singleJson.display_name) {
                            convertedJSON[singleJson.display_name] = singleJson.value;
                        } else {
                            convertedJSON[singleJson.name] = singleJson.value;
                        }
                    }


                    if (Object.keys(convertedJSON).length) {
                        console.log("Object.keys(convertedJSON).length", Object.keys(convertedJSON).length);
                        
                        newConvertedJson.convertedJSONs.push(convertedJSON);
                    }
                }
            }

            // if (!newConvertedJson.end)
            {
               newConvertedJson.start = newConvertedJson.org_start;
               newConvertedJson.end = newConvertedJson.start;
               
                // newConvertedJson.end = moment(newConvertedJson.start).add(3,'hours').format('YYYY-MM-DD HH:mm:ss');
                // console.log(newConvertedJson.start);
            }
            return resolve(newConvertedJson);
        } catch(e) {
            console.log("Error while we are parcing meta json", e);
            return reject(e);            
        }
    })
};

module.exports.canViewProfile = ( id, userType ) => {
    const query = Database.builder()
    .select()
    .field('*')
    .from('users')
    .where('id = ? and type_id = ?',id,userType)
    .toParam();
    if(query.values)
    {
         Database.executeQuery(query);
    }
   
    return true;
}

module.exports.getKeyValueJsonForProfile = (profileJson, id) => {
    return new Promise((resolve, reject) => {
        try {
            const convertedJSON = profileJson.map((_item) => {

                const convertedObj = {};

                if (_item.tag === 'custom') {
                    const arr =
                        _item.children.filter((_child) => _child.value || _child.placeholder)
                            .map(_child => {
                                let obj = {};
                                obj[_child.name] = _child.value || _child.placeholder;
                                console.log(obj);
                                return obj;
                            });
                    convertedObj[_item.title] = arr;
                } else if (_item.tag === 'title') {
                    convertedObj['title'] = _item.title;
                } else {
                    convertedObj[_item.name] = _item.value;
                }
                return convertedObj;

            });
            console.log(JSON.stringify(convertedJSON));
            return resolve(convertedJSON);
            

        }
        catch (e) {
            console.log("Error while we are parcing meta json", e);
            return reject(e);
        }
    })
}


module.exports.canViewEvent = (userType, eventType) => {
     
    if(userType ==='Author'||userType ==='Blogger'||userType ==='Blogger Premium'||userType ==='PR Firm')
    {
        console.log('All the events Visiable' +userType);
       return true;    
    }
    else if(userType ==='Vendor')
    {
        
        console.log('event type 3 not visiable' +userType);
        return true;
    }
    else if(userType === 'Reader')
    {
        console.log('event type 3 not visiable' +userType);
        return true;
    }
    else 
    {
       return false;
    }
   
};

module.exports.canAddEvent = (userType, eventType) => {
  /// const arr = ['Book release','Cover reveal Promo','Cover reveal','ARC sign-up','Release party','Giveaway','Book signing'];
    if(userType === 'Author')
    {
        console.log("Book release Cover reveal Promo Cover reveal ARC sign-up Release party Giveaway Book signing");
        return true;    
    }
    else if(userType === 'Blogger Premium' && (eventType === 'event_type_4' ||eventType ==='event_type_2' || eventType ==='event_type_6'))
    {

        console.log("Release party Giveaway Book signing");
        return true;
    }
    else if(userType === 'PR Firm' && (eventType === 'event_type_3' ||eventType === 'event_type_5' ||eventType === 'event_type_7'  ||eventType ==='event_type_2' || eventType ==='event_type_6'))  
    { 
       console.log("Cover reveal PromoCover reveal ARC sign-up Release party Giveaway Book signing"); 
       return true;
    }
    else if(userType ==='Vendor' && (eventType ==='event_type_6'))
    {
        console.log('Book signing');
        return true;
    }
    else{
        return false;
    }
   
};

