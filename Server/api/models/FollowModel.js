const Database = require('../services/Database.js');

module.exports.checkValidityOfFollowCode = (follow_code) => {
    const query = Database.builder()
        .select()
        .field('id')
        .from('users')
        .where('follow_code = ?', follow_code)
        .toParam();
    
    return Database.executeQuery(query)
    .then((followUSer) => {
        if (followUSer.length) {
            return followUSer[0];
        } else {
            throw ({
                status: 400,
                message: 'Invalid code'
            })
        }
    })
    .catch((error) => {
        throw error;
    })
};

module.exports.checkValidityForNewFollow = (following_user_id, follower_user_id) => {
    const query = Database.builder()
        .select()
        .field('id')
        .from('follow')
        .where('following_user_id = ?', following_user_id)
        .where('user_id = ?', follower_user_id)
        .toParam();
    
    return Database.executeQuery(query);
};