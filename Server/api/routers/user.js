// npm modules
const multer = require('multer');
const uuid = require('uuid');

// Middleware
const isAuthorised = require('../middlewares/isAuthorized');

// Controller
const UserController = require('../controllers/UserController.js');

// Validations
const UserValidations = require('../validations/UserValidations.js');

// File uploading
const upload_profile_photo_store = multer.diskStorage({
  destination(req, file, callback) {
    callback(null, './static/profile-photo');
  },
  filename(req, file, callback) {
    const extension = file.originalname.split('.').slice(-1).pop();
    const final_filename = `${uuid.v4()}.${extension}`;
    callback(null, final_filename);
  },
});

const upload_profile_photo = multer({ storage: upload_profile_photo_store });


const routes = (app) => {
  // Get all users
  app.get(`${process.env.API_PREFIX}/users`, [isAuthorised], UserController.getAllUsers);
  // Signup step
  app.post(`${process.env.API_PREFIX}/signup/step/1`, [UserValidations.signupStep1], UserController.signupStep1);
  // Resend OTP
  app.post(`${process.env.API_PREFIX}/resend/otp`, [UserValidations.resendOtp], UserController.resendOtp);
  // verify Email
  app.put(`${process.env.API_PREFIX}/verify/email`, [UserValidations.verifyEmail], UserController.verifyEmail);
  // Login user
  app.put(`${process.env.API_PREFIX}/pay/membership/:userId`, UserController.addPaymentDetails);
  // Login user
  app.post(`${process.env.API_PREFIX}/login`, [UserValidations.login], UserController.login);
  // Forgot password
  app.post(`${process.env.API_PREFIX}/forgot/password`, [UserValidations.forgotPassword], UserController.forgotPassword);
  // Update pasword via forgot password link
  app.put(`${process.env.API_PREFIX}/reset/password`, [UserValidations.resetPassword], UserController.resetPassword);
  // Add basic details as per user type
  app.put(`${process.env.API_PREFIX}/profile/setup`, [isAuthorised, upload_profile_photo.single('profile')],
    (req, res) => UserController.profileSetUp(req, res));
  // Get user profile
  app.get(`${process.env.API_PREFIX}/user/profile`, [isAuthorised], UserController.getUserProfile);
};

module.exports = { routes };
