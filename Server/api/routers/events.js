// npm modules
const multer = require('multer');
const uuid = require('uuid');

// Middleware
const isAuthorised = require('../middlewares/isAuthorized');

// Controller
const EventController = require('../controllers/EventController.js');

// Validations
const EventValidations = require('../validations/EventValidations.js');

// File uploading
const upload_event_photo_store = multer.diskStorage({
  destination(req, file, callback) {
    callback(null, './static/events');
  },
  filename(req, file, callback) {
    const extension = file.originalname.split('.').slice(-1).pop();
    const final_filename = `${uuid.v4()}.${extension}`;
    callback(null, final_filename);
  },
});

const upload_event_photo = multer({ storage: upload_event_photo_store });


const routes = (app) => {
    // Add event
    app.post(
        `${process.env.API_PREFIX}/event`,
        [isAuthorised, upload_event_photo.single('avtar')],
        (req, res) => EventController.addNewEvent(req, res)
    );
    
    // Get all event by user
    app.get(`${process.env.API_PREFIX}/events`, [isAuthorised], EventController.getEventsByUser);

    // Get particular event details
    app.get(`${process.env.API_PREFIX}/event/:eventId`, [isAuthorised], EventController.getSingleEventDetails);

    // update
    app.put(
        `${process.env.API_PREFIX}/event/:eventId`,
        [isAuthorised, upload_event_photo.single('avtar')],
        (req, res) => EventController.updateEvent(req, res)
    );

    // delete particular event
    app.delete(`${process.env.API_PREFIX}/event/:eventId`, [isAuthorised], EventController.deleteEvent);
 };

module.exports = { routes };
