// Middleware
const isAuthorised = require('../middlewares/isAuthorized');

// Controller
const FollowController = require('../controllers/FollowController.js');

// Validations
const FollowValidations = require('../validations/FollowValidations.js');

const routes = (app) => {
    // Add new follower in signup process
    app.post(
        `${process.env.API_PREFIX}/signup/follow`,
        [FollowValidations.addNewFollowingUserWhileSignup],
        FollowController.addNewFollowingUser
    );

    // Add new following for logged in user
    app.post(
        `${process.env.API_PREFIX}/follow`,
        [isAuthorised, FollowValidations.addNewFollowingUser],
        FollowController.addNewFollowingUserForLoggedInUser
    );

    // Check follow code
    app.get(
        `${process.env.API_PREFIX}/follow/check/validity/:follow_code`,
        FollowController.checkValidiityOfFollowCode
    );

    // delete following user
    app.delete(
        `${process.env.API_PREFIX}/follow/:follow_id`,
        [isAuthorised],
        FollowController.deleteFollowingUser
    );
};

module.exports = { routes };
