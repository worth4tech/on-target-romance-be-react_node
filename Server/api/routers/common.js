// Controller
const CommonController = require('../controllers/CommonController.js');

const routes = (app) => {
  // Get country list
  app.get(`${process.env.API_PREFIX}/country`, CommonController.getCountryList);
  // Get all user type list
  app.get(`${process.env.API_PREFIX}/user-type`, CommonController.getUserTypeList);
  // Fetch membership details
  app.get(`${process.env.API_PREFIX}/membership/details/:memberShipID`, CommonController.fetchMemberShipDetails);
  // Fetch form-schema details
  app.get(`${process.env.API_PREFIX}/form-schema/:formSchemaName`, CommonController.fetchFormSchema);
  // Update form-schema details
  app.put(`${process.env.API_PREFIX}/form-schema/:formSchemaName`, CommonController.updateMetaJson);
};

module.exports = { routes };
