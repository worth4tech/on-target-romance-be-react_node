// models
const CommonQueryModel = require('../models/commonQueryModel');
const UserModel = require('../models/UserModel');

// Get country list
module.exports.getCountryList = (req, res) => {
  console.log('Inside the get country list controller function');
  CommonQueryModel.selectRecord(
    ['*'],
    [],
    'county_master',
    'name'
  )
    .then(countryData => res.status(200).json({ message: 'Data fetched', result: { countries: countryData } }))
    .catch(error => res.status(error.status || 500).json({ message: error.message || 'Some internal server error' }));
};
// Get user type list
module.exports.getUserTypeList = (req, res) => UserModel.fetchUserTypeWithDescription()
  .then(userTypeData => res.status(200).json({ message: 'Data fetched', result: { userTypes: userTypeData } }))
  .catch(error => res.status(error.status || 500).json({ message: error.message || 'Some internal server error' }));


// Get membership details
module.exports.fetchMemberShipDetails = (req, res) => CommonQueryModel.selectRecord(
  ['description'],
  [
    {
      key: 'user_type_id',
      value: req.params.memberShipID,
    },
  ],
  'membership_master'
)
  .then((memberShipDescription) => {
    if (memberShipDescription.length) {
      return res.status(200).json({
        message: 'Data fetched',
        result: {
          details: memberShipDescription[0].description,
        },
      });
    }
    // No record found with this memberShipId
    throw Object.assign(
      new Error('Invalid membership data'),
      { status: 400 }
    );
  })
  .catch(error => res.status(error.status || 500).json({ message: error.message || 'Some internal server error' }));

module.exports.fetchFormSchema = (req, res) => {
  const os = req.headers && req.headers['os'] ? `os = '${req.headers['os']}'` : `os is null`;
  const platform = req.headers && req.headers['platform'] ? req.headers['platform'] : 'web';

  CommonQueryModel.selectRecord(
    ['*'],
    [
      {
        key: 'form_schema_name',
        value: req.params.formSchemaName,
      },
      {
        key: os,
      },
      {
        key: 'platform',
        value: platform,
      },
    ],
    'form_schema'
  )
    .then(data => res.status(200).json({ result: { schema: data && data.length && data[0].form_schema ? JSON.parse(data[0].form_schema) : {} } }))
    .catch(error => res.status(error.status || 500).json({ message: error.message || 'Some internal server error' }));
};

module.exports.updateMetaJson = (req, res) => {
  return CommonQueryModel.selectRecord(
    ['id'],
    [
      {
        key: 'form_schema_name',
        value: req.params.formSchemaName,
      }
    ],
    'form_schema'
  )
  .then((data) => {
    if (!data.length) {
      throw ({
        status: 403,
        message: "We don't have this form json data"
      });
    } else {
      return CommonQueryModel.updateRecord(
        { 
          form_schema: JSON.stringify(req.body.form_schema)
        },
        [
          {
              key: 'form_schema_name',
              value: req.params.formSchemaName,
          }
        ],
        'form_schema'
      );
    }
  })
  .then(() => res.status(200).json({ message: "json schema updated" }))
  .catch(error => res.status(error.status || 500).json({ message: error.message || 'Some internal server error' }));
};