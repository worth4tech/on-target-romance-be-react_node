// Third party library
const moment = require('moment');

// models
const CommonQueryModel = require('../models/commonQueryModel');
const EventModel = require('../models/EventModel');
const UserModel = require('../models/UserModel');

const event_name = {
    'event_type_1': 'Book Release',
    'event_type_2': 'Giveaway',
    'event_type_3': 'Cover Reveal Promo',
    'event_type_4': 'Release Party',
    'event_type_5': 'Cover Reveal',
    'event_type_6': 'Book Signing',
    'event_type_7': 'ARC Signup'
};

// Add new event
module.exports.addNewEvent = (req, res) => {
    const type = req.body.event_type;
    UserModel.fetchFullUserProfile(req.id)
        .then(async (userData) => {
            if (userData && userData.length) {
                const userType = userData[0].type_name;
                const eventType = type;
                let fname = userData[0].fname;
                let lname = userData[0].lname;
                const bool = EventModel.canAddEvent(userType, eventType);
                const filePaths = [];
                // console.log(bool);

                if (bool === true) {
                    try {
                        const orgMetaJSON = JSON.parse(req.body.event_details);
                        let filePath = '';

                        // File is uploaded on it's location but need to push path into meta json
                        if (req.file && Object.keys(req.file).length > 0) {
                            console.log("File attached", req.file);
                            for (let i = 0; i < orgMetaJSON.length; i += 1) {
                                if (orgMetaJSON[i].type === 'file') {
                                    orgMetaJSON[i].value = req.file.path.replace('static', '');
                                    filePath = req.file.path.replace('static', '');
                                    filePaths[0] = filePath;
                                    break;
                                }
                            }
                        }

                        // We need to deleted avatar because we are not storing into different column
                        delete req.body.avtar;

                        // Assign loggein userId to this event
                        req.body['user_id'] = req.id;

                        // Sort array according to index key
                        orgMetaJSON.sort((a, b) => {
                            return (a.index ? a.index : 0) - (b.index ? b.index : 0);
                        });

                        return EventModel.getKeyValueObjectFromMetaJson(orgMetaJSON,eventType)
                            .then((convertedJSON) => {
                                console.log("convertedJSON", convertedJSON);
                                const event_details_with_values = convertedJSON.convertedJSONs;
                               
                                req.body.title = convertedJSON.title;
                                req.body.start_date = convertedJSON.start;
                                req.body.end_date = convertedJSON.end;
                               
                                console.log("filePaths", filePaths);

                                if (filePaths && filePaths.length) {
                                    event_details_with_values.push({
                                        image: filePaths[0]
                                    });
                                }
                                /*  var myDate = convertedJSON.start; 

                              if (convertedJSON.start) {
                                  event_details_with_values['start_date'] = moment(myDate).format('YYYY-MM-DD hh:mm:ss');

                             //     // Check if end date extist then convert it into YYYY-MM-DD format orherwise end_date = start_date
                                  if (convertedJSON.end) {
                                      event_details_with_values['end_date'] = moment(myDate).add(3,'hours').format('YYYY-MM-DD hh:mm:ss');
                                  } else {
                                      event_details_with_values['end_date'] = moment(myDate).add(3,'hours').format('YYYY-MM-DD hh:mm:ss');
                                  }
                              } else {
                                 event_details_with_values['start_date'] = null;
                                  event_details_with_values['end_date'] = null;
                              }
                              console.log(event_details_with_values['start_date']);
                              console.log(event_details_with_values['end_date']);*/

                                const objFname = event_details_with_values.find((_value) => _value['Author first name']);
                                const objLname = event_details_with_values.find((_value) => _value['Author last name']);
                                
                                if (objFname) {
                                    fname = objFname['Author first name'];
                                }

                                if (objLname) {
                                    lname = objLname['Author last name'];
                                }

                                let fnameIndex = -1;
                                event_details_with_values.forEach((_value, index) => {
                                    if (_value['Author first name']) {
                                        fnameIndex = index;
                                    }
                                });

                                if (fnameIndex > -1) {
                                    event_details_with_values.splice(fnameIndex, 2);
                                }

                                if (fname && fname.length) {
                                    req.body['author_first_name'] = fname;
                                    req.body['author_last_name'] = lname;
                                }

                                if(fname && fname.length) {
                                    event_details_with_values.splice(1, 0, {
                                        'Author name': `${fname} ${lname}`, 
                                    });
                                }

                                // console.log("event_details_with_values ---->", event_details_with_values);

                                req.body.event_details_with_values = JSON.stringify(event_details_with_values);
                                req.body.event_details = JSON.stringify(JSON.parse(req.body.event_details));
                                return;
                            })
                            .then(() => {
                                return CommonQueryModel.addNewRecord([req.body], 'events');
                                // return res.status(200).json(JSON.parse(req.body.event_details_with_values));
                            })
                            .then(() => res.status(200).json({
                                message: "Event created successfully"
                            }))
                            .catch((error) => {
                                console.log(error);
                                return res.status(error.status ? error.status : 500).json({
                                    message: error
                                })
                            })
                    } catch (e) {
                        console.log("Error while we are going to create a new event", e);
                        return res.status(error.status ? error.status : 500).json({
                            message: error
                        });
                    }

                } else {
                    return res.status(403).json({
                        message: "You are not Authorized",

                    })
                }
            }

        });



};

// Fetch single event
module.exports.getSingleEventDetails = (req, res) => {
    return CommonQueryModel.selectRecord(
        ['*'],
        [
            {
                key: 'user_id',
                value: req.id
            },
            {
                key: 'id',
                value: req.params.eventId
            }
        ],
        'events'
    )
        .then((events) => {
            if (events && events.length) {
                events[0]['event_details'] = JSON.parse(events[0]['event_details']);
                return res.status(200).json({
                    message: "Event detail fetched",
                    result: {
                        event: events[0]
                    }
                })
            } else {
                throw ({
                    status: 403,
                    message: "Event not found"
                });
            }
        })
        .catch((error) => {
            return res.status(error.status ? error.status : 500).json({
                message: error.message || "Internal server error"
            });
        })
};

// Fetch all the events of particular user
module.exports.getEventsByUser = (req, res) => {
    UserModel.fetchFullUserProfile(req.id)
        .then(async (userData) => {
            if (userData && userData.length) {
                const userType = userData[0].type_name;

                return EventModel.getEventsByUserById(req.id)
                    .then(async (events) => {

                        const filteredEvents = events.filter((_e) => {
                            const eventType = _e.event_type;
                            return EventModel.canViewEvent(userType, eventType);
                        });

                        return res.status(200).json({
                            message: "Event fetched successfully",
                            result: {

                                events: filteredEvents.map((singleEvent) => {
                                    let arr = [];
                                    if (!singleEvent['event_details_with_values']) {
                                        console.log('No data');
                                    }

                                    else {
                                        if (singleEvent['event_type'] === 'event_type_7') {
                                            // remaining reder and blogger radio check   
                                            singleEvent['event_details_with_values'] = JSON.parse(singleEvent['event_details_with_values']);
                                        }
                                        else {
                                            singleEvent['event_details_with_values'] = JSON.parse(singleEvent['event_details_with_values']);
                                        }

                                        singleEvent['event_type'] = event_name[singleEvent['event_type']];
                                        return singleEvent;
                                    }

                                })
                            }
                        });
                    }).catch((error) => {
                        return res.status(error.status ? error.status : 500).json({
                            message: error.message || "Internal server error"
                        });
                    })




            }

        });
    /*  return EventModel.getEventsByUser(req.id)
          .then(async (events) => {
              return res.status(200).json({
                  message: "Event fetched successfully",
                  result: {
                      // events
                      events: events.map((singleEvent) => {
                          if (singleEvent['event_details_with_values']) {
                              singleEvent['event_details_with_values'] = JSON.parse(singleEvent['event_details_with_values']);
                          }
                          return singleEvent;
                      })
                  }
              });
          })
          .catch((error) => {
              return res.status(error.status ? error.status : 500).json({
                  message: error.message || "Internal server error"
              });
          })*/
};

// Delete single event
module.exports.deleteEvent = (req, res) => {
    return CommonQueryModel.selectRecord(
        ['created_date'],
        [
            {
                key: 'user_id',
                value: req.id
            },
            {
                key: 'id',
                value: req.params.eventId
            }
        ],
        'events'
    )
        .then((events) => {
            if (events && events.length) {
                return CommonQueryModel.deleteRecord(
                    [
                        {
                            key: 'id',
                            value: req.params.eventId
                        },
                        {
                            key: 'user_id',
                            value: req.id
                        }
                    ],
                    'events'
                )
            } else {
                throw ({
                    status: 403,
                    message: "Event not found. May be alredy deleted"
                });
            }
        })
        .then(() => {
            return res.status(200).json({
                message: "Event deleted",
            })
        })
        .catch((error) => {
            return res.status(error.status ? error.status : 500).json({
                message: error.message || "Internal server error"
            });
        })
}

// Update event
module.exports.updateEvent = (req, res) => {
    return CommonQueryModel.selectRecord(
        ['created_date'],
        [
            {
                key: 'user_id',
                value: req.id
            },
            {
                key: 'id',
                value: req.params.eventId
            }
        ],
        'events'
    )
        .then((events) => {
            if (events && events.length) {
                return CommonQueryModel.updateRecord(
                    { event_details: req.body.event_details, event_type: req.body.event_type },
                    [
                        {
                            key: 'id',
                            value: req.params.eventId,
                        },
                        {
                            key: 'user_id',
                            value: req.id
                        },
                    ],
                    'events'
                )
            } else {
                throw ({
                    status: 403,
                    message: "Event not found"
                });
            }
        })
        .then(() => {
            return res.status(200).json({
                message: "Event updated"
            });
        })
        .catch((error) => {
            return res.status(error.status ? error.status : 500).json({
                message: error.message || "Internal server error"
            });
        })
};