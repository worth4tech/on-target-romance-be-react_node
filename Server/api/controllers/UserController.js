// npm modules
const moment = require('moment');

// models
const CommonQueryModel = require('../models/commonQueryModel');
const UserModel = require('../models/UserModel');
const EventModel = require('../models/EventModel');

// Services
const AuthService = require('../services/AuthService');
const PasswordService = require('../services/PasswordService');
const EmailServices = require('../services/MailService');

// Initial signup with basic user details
module.exports.signupStep1 = (req, res) => CommonQueryModel.selectRecord(
	['*'],
	[
		{
			key: 'email',
			value: req.body.email,
		},
	],
	'users'
)
	.then(async (userDetails) => {
		if (userDetails.length) {
			throw Object.assign(
				new Error('User with same email already exists'),
				{ status: 422 }
			);
		}

		// Need to check for username. If username is already exist then we will not allow
		return CommonQueryModel.selectRecord(
			['id'],
			[
				{
					key: 'display_name',
					value: req.body.display_name,
				},
			],
			'users'
		);
	})
	.then(async (userNameAlreadyExist) => {
		if (userNameAlreadyExist && userNameAlreadyExist.length) {
			throw Object.assign(
				new Error('User with same username already exists'),
				{ status: 422 }
			);
		}

		// Enter user into database
		req.body.password = await PasswordService.generatePasswordHash(req.body.password);
		req.body.follow_code = await AuthService.randomString(process.env.FOLLOW_CODE_LENGTH);

		return CommonQueryModel.addNewRecord([req.body], 'users');
	})
	.then(async () => {
		// Create record into otp table with OTP
		req.body.otp = await AuthService.generateOTP();
		return CommonQueryModel.addNewRecord(
			[
				{
					email: req.body.email,
					expiry_at: moment(new Date()).add(parseInt(process.env.OTP_EXPIRY_TIME, 10), 'minutes').format('YYYY-MM-DD HH:mm:ss'),
					otp: req.body.otp,
				},
			],
			'otp'
		);
	})
	.then(async () => {
		const emailData = {
			display_name: req.body.display_name,
			otp: req.body.otp,
		};

		return EmailServices.sendEmail(
			process.env.GMAIL_USER_NAME,
			req.body.email,
			'Account verification',
			'otp-send',
			emailData
		);
	})
	.then(() => res.status(200).json({
		message: 'Signup successfull',
	}))
	.catch(error => res.status(error.status || 500).json({
		message: error.message || 'Something went wrong. Please try again.',
	}));

// Resend OTP
module.exports.resendOtp = async (req, res) => {
	const { email } = req.body;
	const otp = await AuthService.generateOTP();

	return CommonQueryModel.updateRecord(
		{
			otp,
			expiry_at: moment(new Date()).add(parseInt(process.env.OTP_EXPIRY_TIME, 10), 'minutes').format('YYYY-MM-DD HH:mm:ss'),
		},
		[
			{
				key: 'email',
				value: email,
			},
		],
		'otp'
	)
		.then(() => EmailServices.sendEmail(
			process.env.GMAIL_USER_NAME,
			email,
			'Account verification',
			'otp-send',
			{
				email,
				otp,
			}
		))
		.then(() => res.status(200).json({
			status: true,
			message: 'OTP sent to your email',
		}))
		.catch(error => res.status(error.status || 500).json({
			message: error.message || 'Something went wrong. Please try again.',
		}));
};

// Verify email
module.exports.verifyEmail = (req, res) => CommonQueryModel.selectRecord(
	['expiry_at'],
	[
		{
			key: 'email',
			value: req.body.email,
		},
		{
			key: 'otp',
			value: req.body.otp,
		},
	],
	'otp'
)
	.then((otpDetails) => {
		if (otpDetails.length) {
			// OTP with this email is exists but need to check for expiry
			const time_difference = moment(otpDetails[0].expiry_at).diff(moment(new Date()), 'minutes');
			if (
				time_difference > 0
				&& time_difference < parseInt(process.env.OTP_EXPIRY_TIME, 10)
			) {
				// Here we need to update is_email_verified = true in users table with this email
				return CommonQueryModel.updateRecord(
					{ is_email_verified: true },
					[
						{
							key: 'email',
							value: req.body.email,
						},
					],
					'users'
				);
			}

			// OTP with this email is expired
			throw Object.assign(
				new Error('OTP expired'),
				{ status: 406 }
			);
		} else {
			// OTP with this email is not exists
			throw Object.assign(
				new Error('OTP not match with email'),
				{ status: 406 }
			);
		}
	})
	.then(() => CommonQueryModel.deleteRecord(
		[
			{
				key: 'email',
				value: req.body.email,
			},
		],
		'otp'
	))
	.then(() => CommonQueryModel.selectRecord(
		['id'],
		[
			{
				key: 'email',
				value: req.body.email,
			},
		],
		'users'
	))
	.then(userData => res.status(200).json({
		message: 'OTP verified',
		result: {
			data: {
				userId: userData[0].id,
			},
		},
	}))
	.catch(error => res.status(error.status ? error.status : 500).json({
		message: error.message || 'Something went wrong. Please try again.',
	}));

// Login
module.exports.login = (req, res) => CommonQueryModel.selectRecord(
	['*'],
	[
		{
			key: 'email',
			value: req.body.email,
		},
	],
	'users'
)
	.then(async (userDetails) => {
		if (userDetails.length) {
			// Check for email verification
			if (!userDetails[0].is_email_verified) {
				throw Object.assign(
					new Error('E-mail not verified'),
					{ status: 403 }
				);
			}

			// User exists. Need to check for password.
			if (await PasswordService.checkPassword(req.body.password, userDetails[0].password)) {
				return res.status(200).json({
					message: 'Login successfull',
					token: AuthService.generateToken({
						id: userDetails[0].id,
					}),
				});
			}

			// Password not match
			throw Object.assign(
				new Error('Invalid password.'),
				{ status: 422 }
			);
		}

		// User with this email not exists
		throw Object.assign(
			new Error('Email does not exists.'),
			{ status: 422 }
		);
	})
	.catch(error => res.status(error.status || 500).json({
		message: error.message || 'Something went wrong. Please try again.',
	}));

// Forgot password
module.exports.forgotPassword = async (req, res) => {
	const forgotPassword = await AuthService.randomString(process.env.FORGOT_PASSWORD_STRING_LENGTH);
	let userDetails = {};

	return CommonQueryModel.selectRecord(
		['*'],
		[
			{
				key: 'email',
				value: req.body.email,
			},
		],
		'users'
	)
		.then(async (userData) => {
			if (userData.length) {
				userDetails = userData;
				// User found with this email
				return CommonQueryModel.updateRecord(
					{
						forgot_password_code: forgotPassword,
					},
					[
						{
							key: 'email',
							value: req.body.email,
						},
					],
					'users'
				);
			}

			// User with this record not match
			throw Object.assign(
				new Error('Email not found in our system'),
				{ status: 422 }
			);
		})
		.then(() => {
			const emailData = {
				display_name: userDetails.display_name,
				forgot_password_code: forgotPassword,
			};

			return EmailServices.sendEmail(
				process.env.GMAIL_USER_NAME,
				req.body.email,
				'Forgot password',
				'forgot-password',
				emailData
			);
		})
		.then(() => res.status(200).json({
			message: 'Please check your email. We sent link for change passowrd',
		}))
		.catch(error => res.status(error.status || 500).json({
			message: error.message || 'Something went wrong. Please try again.',
		}));
};

module.exports.resetPassword = (req, res) => CommonQueryModel.selectRecord(
	['*'],
	[
		{
			key: 'forgot_password_code',
			value: req.body.forgot_password_code,
		},
	],
	'users'
)
	.then(async (userData) => {
		if (userData.length) {
			return CommonQueryModel.updateRecord(
				{
					password: await PasswordService.generatePasswordHash(req.body.password),
					forgot_password_code: null,
					is_email_verified: true,
				},
				[
					{
						key: 'forgot_password_code',
						value: req.body.forgot_password_code,
					},
				],
				'users'
			);
		}

		// No record found with this forgot_password_code
		throw Object.assign(
			new Error('Link expired'),
			{ status: 403 }
		);
	})
	.then(() => res.status(200).json({ message: 'Password changed' }))
	.catch(error => res.status(error.status || 500).json({
		message: error.message || 'Something went wrong. Please try again.',
	}));


// Add user more data into users table with
module.exports.profileSetUp = (req, res) => {
	UserModel.fetchFullUserProfile(req.id)
	          .then(async(userData)=>{
				
				  if(userData && userData.length){
					  const userType = userData[0].type_name;
					  const id = req.id;
					  const bool = EventModel.canViewProfile(id,userType);
					  console.log(bool);
					 if(bool===true)
					 {
						const orgMetaJSON = JSON.parse(req.body.details);
						if (req.file && Object.keys(req.file).length > 0) {
							for (let i = 0; i < req.body.details.length; i += 1) {
								if (req.body.details[i].type === 'file') {
									req.body.details[i].value = req.file.path.replace('static', '');
								}
								// For mobile application
								if (req.body.details[i] && req.body.details[i].tag === 'UserProfileImageURL') {
									req.body.details[i].value = req.file.path.replace('static', '');
								}
							}
						}
					//	const profile_details_with_values = convertedJSON;
						return EventModel.getKeyValueJsonForProfile(orgMetaJSON,id)
						.then((convertedObj) =>{
							console.log(convertedObj)
								const profile_details_with_values = convertedObj;
								const fname = convertedObj[0].firstname;
								const lname = convertedObj[1].lastname;

	                         	return CommonQueryModel.updateRecord(
									{
										user_details: JSON.stringify(JSON.parse(req.body.details)),
										profile_details_with_values: JSON.stringify(profile_details_with_values),
										fname:fname,
										lname:lname
									},
									[
										{
											key: 'id',
											value: req.id,
										},
									],
									'users'
								)
									.then(() => res.status(200).json(
										{ message: 'Profile setup successfully'}))
									.catch(error => res.status(error.status || 500).json({
										message: error.message || 'Something went wrong. Please try again.',
									}));
							}
						
								
						)
					
						}
				  }
			  });

	
}

// add payment details into transaction table and update membership in users table
module.exports.addPaymentDetails = (req, res) => {
	const userDetails = {
		validity: 0,
	};

	return UserModel.fetchUserDetailsWithMemberShip(req.params.userId)
		.then((userData) => {
			if (userData.length && userData[0].id) {
				userDetails.validity = userData[0].validity;
				return CommonQueryModel.addNewRecord(
					[
						{
							membership_id: userData[0].id,
							user_id: parseInt(req.params.userId, 10),
							stage: 'complete',
							transaction_id: 'ABCD123',
							reference_id: 'ABCD123',
							status: 'success',
						},
					],
					'transaction_logs'
				);
			}
		})
		.then(() => CommonQueryModel.updateRecord(
			{
				member_expiry_date: moment(new Date()).add(365 * userDetails.validity, 'days').format('YYYY-MM-DD HH:mm:ss'),
				updated_date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
			},
			[
				{
					key: 'id',
					value: req.params.userId,
				},
			],
			'users'
		))
		.then(() => res.status(200).json({ message: 'Payment done' }))
		.catch(error => res.status(error.status || 500).json({
			message: error.message || 'Something went wrong. Please try again.',
		}));
};

module.exports.getUserProfile = (req, res) => UserModel.fetchFullUserProfile(req.id)
	.then(async (userData) => {
		if (userData && userData.length) {
			delete userData[0].password;
			delete userData[0].forgot_password_codel;

			userData[0].user_details = userData[0].user_details !== null ? JSON.parse(userData[0].user_details) : [];

			if (userData[0].user_details.length === 0) {
				userData[0]['profile_setup_schema'] = await UserModel.getUserSchema(userData[0].type_name);
			}

			return res.status(200).json({
				message: 'Profile fetch',
				result: {
					profile: userData[0],
				},
			});
		}

		return res.status(422).json({
			message: 'User not found',
		});
	})
	.catch(error => res.status(error.status || 500).json({
		message: error.message || 'Something went wrong. Please try again.',
	}));

module.exports.getAllUsers = (req, res) => {
	const limit = req.query && req.query.limit ? Number(req.query.limit) : 10;
	const skip = req.query && req.query.skip ? Number(req.query.skip) : 0;
	const search = req.query && req.query.search ? req.query.search : null;
	const userType = req.query && req.query.userType ? Number(req.query.userType) : 0;

	return UserModel.getAllUsers(limit, skip, userType, search)
		.then((userData) => res.status(200).json({
			message: "User data fetch successfully",
			result: {
				userData 
			}
		}))
		.catch(error => res.status(error.status || 500).json({
			message: error.message || 'Something went wrong. Please try again.',
		}));
};