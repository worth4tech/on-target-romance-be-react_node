// models
const FollowModel = require('../models/FollowModel');
const CommonQueryModel = require('../models/commonQueryModel');

module.exports.addNewFollowingUser = (req, res) => {
    // First we need to validity for this users in follow table. Either there is possibility user is already follow following user
    return FollowModel.checkValidityForNewFollow(req.body.following_user_id, req.body.user_id)
    .then((followingUser) => {
        if (followingUser.length) {
            // This user is already follow to following user.
            throw ({
                status: 403,
                message: 'You are already follow'
            });
        } else {
            // Need to create a new record in follow table for this follower and following user
            return CommonQueryModel.addNewRecord(
                [
                    {
                        user_id: req.body.user_id,
                        following_user_id: req.body.following_user_id
                    },
                ],
                'follow'
            );
        }
    })
    .then(() => res.status(200).json({
        canFollow: true,
        message: 'Succefuly follow'
    }))
    .catch(error => res.status(error.status || 500).json({
		message: error.message || 'Something went wrong. Please try again.',
	}));
};

module.exports.addNewFollowingUserForLoggedInUser = (req, res) => {
    // First we need to validity for this users in follow table. Either there is possibility user is already follow following user
    return FollowModel.checkValidityForNewFollow(req.body.following_user_id, req.id)
    .then((followingUser) => {
        if (followingUser.length) {
            // This user is already follow to following user.
            throw ({
                status: 403,
                message: 'You are already follow'
            });
        } else {
            // Need to create a new record in follow table for this follower and following user
            return CommonQueryModel.addNewRecord(
                [
                    {
                        user_id: req.id,
                        following_user_id: req.body.following_user_id
                    },
                ],
                'follow'
            );
        }
    })
    .then(() => res.status(200).json({
        canFollow: true,
        message: 'Succefully follow'
    }))
    .catch(error => res.status(error.status || 500).json({
		message: error.message || 'Something went wrong. Please try again.',
	}));
};

module.exports.checkValidiityOfFollowCode = (req, res) => {
    // First we need to check follow code is valid or not
    return FollowModel.checkValidityOfFollowCode(req.params.follow_code)
    .then((followingUser) => {
        return res.status(200).json({
            canFollow: true,
            message: 'Valid follow code',
            following_user_id: followingUser.id
        })
    })
    .catch(error => res.status(error.status || 500).json({
        canFollow: false,
		message: error.message || 'Something went wrong. Please try again.',
	}));
};

module.exports.deleteFollowingUser = (req, res) => {
    return CommonQueryModel.selectRecord(
        ['id', 'user_id', 'following_user_id'],
        [
            {
                key: 'user_id',
                value: req.id
            },
            {
                key: 'id',
                value: req.params.follow_id
            }
        ],
        'follow'
    )
    .then((followRecord) => {
        if (followRecord && followRecord.length) {
            return CommonQueryModel.deleteRecord(
                [
                    {
                        key: 'id',
                        value: req.params.follow_id
                    }
                ],
                'follow'
            )
        } else {
            throw({
                status: 401,
                message: 'Something went terribly wrong'
            })
        }
    })
    .then(() => {
        return res.status(200).json({
            message: "Successfully remove from following list",
        })
    })
    .catch(error => res.status(error.status || 500).json({
        canFollow: false,
		message: error.message || 'Something went wrong. Please try again.',
	}));
};