const chalk = require('chalk');
const express = require('express');
const serveIndex = require('serve-index');
const bodyParser = require('body-parser');
const glob = require('glob');
const path = require('path');

// Load environment variables.
require('dotenv').config();

const fs = require('fs');
const https = require('https');

// const privateKey = fs.readFileSync('certificate/server.key', 'utf8');
// const certificate = fs.readFileSync('certificate/server.cert', 'utf8');
// const credentials = { key: privateKey, cert: certificate };

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use('/images', express.static(__dirname));
app.use('/images', serveIndex(__dirname));
app.use('/', express.static('./static'));
// app.use(express.static(path.join(__dirname, './static')));
app.set('view engine', 'pug');

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-authorization');
  next();
});

app.get(/^\/(?!api).*/, function (req, res) {
  res.redirect('/');
});

const initRoutes = (app) => {
  // including all routes
  glob('./api/routers/*.js', (err, routes) => {
    if (err) {
      console.log('Error occured including routes');
      return;
    }
    routes.forEach((routePath) => {
      require(routePath).routes(app); // eslint-disable-line
    });
    console.warn('No of routes file : ', routes.length);
  });
};

initRoutes(app);

app.listen(process.env.PORT || 3001, () => {
  console.log(chalk.blue(`One-target-romance-app listening on port ${process.env.PORT}!`));
});

// const httpsServer = https.createServer(credentials, app);
// httpsServer.listen(3443);
