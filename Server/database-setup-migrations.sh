#!/bin/bash


# Migration Shell Script
# Migrate SQl files from ./database/migrations/ to Database.

echo '----------------------------------------------------------'
echo 'Migrate OTR Schema into your database, before that please provide access information of you database'
echo '----------------------------------------------------------'
echo 'enter username:'
read username
echo 'enter password:'
read -s password
echo 'enter database name:'
read dbname
echo 'enter host:'
read host

for entry in $PWD/database/database-shema/otr.sql
do
	echo $entry
  mysql -h "$host" -u "$username" --password="$password" "$dbname" < "$entry"
done
echo "all SQL files inside "$PWD/database/migrations/" migrated!"
