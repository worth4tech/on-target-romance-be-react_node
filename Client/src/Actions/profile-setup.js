import axios from 'axios';
import createHistory from 'history/createHashHistory';
import { notify } from 'react-notify-toast';

export default createHistory()
const globalHistory = new createHistory();

export function setupEvent() {
    return (dispatch) => {
        dispatch(profileLoading(false));
    }
}

export function profileLoading(bool) {
    return {
        type: 'PROFILE_LOADING',
        status: bool
    };
}

export function profileError(bool) {
    return {
        type: 'PROFILE_ERROR',
        error: bool
    };
}

export function profileData(data) {
    return {
        type: 'PROFILE_DATA',
        data
    };
}

export function userListData(data) {
    return {
        type: 'USERLIST_DATA',
        data
    };
}

export function userType(user_type) {
    console.log("Inside profile-setup", user_type);
    return {
        type: 'USER_TYPE',
        user_type
    };
}
export function saveProfile(url, headers, data, history) {
    return (dispatch) => {
        dispatch(profileLoading(true));
        axios.put(url, data, { headers: headers })
            .then(response => {
                dispatch(profileLoading(false));
                notify.show("Profile saved successfully", "custom", 2000, { background: '#d4edda', text: "#155724", borderColor: "#c3e6cb" });
                if (history !== null) {
                    history.push("/setup-event");
                }

            })
            .catch(error => {
                dispatch(profileLoading(false));
                notify.show("failed to store user profile", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
                localStorage.removeItem("token");
                history.push("/");
            });
    }

}

export function getProfile(url, headers) {
    return (dispatch) => {
        dispatch(profileLoading(true));
        axios.get(url, headers)
            .then(res => {
                if (res.data.result.profile.user_details.length !== 0) {
                    globalHistory.push('/dashboard');
                }
                if (res.data && res.data.result && res.data.result && res.data.result.profile) {
                    dispatch(userType(res.data.result.profile.type_name));
                }

                if (res.data.result && res.data.result.profile && res.data.result.profile.user_details && res.data.result.profile.user_details.length > 0) {
                    dispatch(profileData(res.data.result.profile.user_details));
                    dispatch(profileLoading(false));
                } else {
                    dispatch(getProfileSchema("profile_type_" + res.data.result.profile.type_id));
                }
            })
            .catch(error => {
                notify.show("failed to store user profile", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
                localStorage.removeItem("token");
                globalHistory.push("/");
            });
    }

}

export function getProfileSchema(schema_name) {
    return (dispatch) => {
        axios.get("/api/v1/form-schema/" + schema_name)
            .then(response => {

                if (response.data.result.schema.length > 0) {
                    dispatch(profileData(response.data.result.schema));
                    dispatch(profileLoading(false));
                }
                if (response.data.result.schema.length === undefined) {
                    globalHistory.push('/dashboard');
                }
            })
            .catch(error => {
                dispatch(profileLoading(false));
                notify.show("something went wrong please reload page or login again.", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
            });
    }

}

export function getUserList(url, headers) {
    return (dispatch) => {
     //   dispatch(profileLoading(true));
        axios.get(url, headers)
            .then(res => {
                if (res.data.result && res.data.result.userData) {
                    dispatch(userListData(res.data.result.userData));
                //    dispatch(profileLoading(false));
                }
            })
            .catch(error => {
                notify.show("failed to store user profile", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
                localStorage.removeItem("token");
             //   dispatch(profileLoading(false));
            });
    }

}