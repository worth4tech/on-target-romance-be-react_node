import axios from 'axios';
import  {notify} from 'react-notify-toast';
export function userlists(token) {
    return {
        type: 'USERS_LOADED',
        token
    };
}
export function userloading(bool) {
    return {
        type: 'USERS_LOADING',
        status: bool
    };
}

export function signuploading(bool) {
    return {
        type: 'SIGNUP_LOADING',
        signup_status: bool
    };
    
}

export function signuppage(number) {
    return {
        type: 'SIGNUP_PAGE',
        page: number
    };
}

export function usererror(bool) {
    return {
        type: 'USERS_ERROR',
        error: bool
    };
}

export function authenticate(url, login, password, router) {
    return (dispatch) => {
        dispatch(userloading(true));
        let data = JSON.stringify({ email: login, password: password });
        axios.post(url, data,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            }
        })
        .then((response) => {
            if (response.data.token) {
                dispatch(usererror(false));
                router.push('/user');
                dispatch(userlists(response.data.token));
                localStorage.setItem("token", response.data.token);
                notify.show("Logged In successfully", "custom", 2000, { background: '#d4edda', text: "#155724",borderColor: "#c3e6cb"});
            }
            dispatch(userloading(false));
        })
        .catch((error) => {
            dispatch(userloading(false));
            // if(error.response.status===401){
            //     notify.show("Username or password is incorrect.", "custom", 5000, { background: '#f8d7da', text: "#721c24"});
            // } 
            notify.show(error.response.data.message, "custom", 5000, { background: '#f8d7da', text: "#721c24"});
             
               
        })
    };
}

export function register(url, name, email, password, country_id,type_id) {
    return (dispatch) => {
        dispatch(signuploading(true));
        let data = JSON.stringify({ type_id: type_id, display_name: name, email: email, password: password, country_id: country_id });
        axios.post(url, data,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            }
        })
        .then((response) => {
            localStorage.setItem("username", email);
            dispatch(signuppage(2));
            dispatch(signuploading(false));
        })
        .catch((error) => {
            dispatch(signuploading(false));
            if(error.response.status===422){
                notify.show(error.response.data.message, "custom", 5000, { background: '#f8d7da', text: "#721c24"});
            }       
        })
    };
}

export function verifyOTP(url, email, otp) {
    return (dispatch) => {
        dispatch(userloading(true));
        let key= JSON.stringify({ email: email, otp: otp });
        
        axios.put(url,key,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            }
        }).then(response =>{
            if(response.status===200){
                localStorage.setItem("i_user",response.data.result.data.userId);
                dispatch(signuppage(3));
            }else
                notify.show("Failed to verify OTP. Please try again with valid OTP.", "custom", 5000, { background: '#f8d7da', text: "#721c24"});
            dispatch(userloading(false));
        }).catch(error =>{
            dispatch(userloading(false));
            notify.show("Failed to verify OTP. Please try again with valid OTP.", "custom", 5000, { background: '#f8d7da', text: "#721c24"});
        })
            
    };
}

export function paymentConfirmation(url) {
    return (dispatch) => {
        dispatch(userloading(true));
        
        axios.put(url+localStorage.getItem("i_user"),{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            }
        }).then(response =>{
            //if(response.status===200){
                localStorage.removeItem("i_user");
                dispatch(signuppage(5));
                notify.show("Payment completed successfully", "custom", 2000, { background: '#d4edda', text: "#155724",borderColor: "#c3e6cb"});
            // }else
            //     notify.show("Transaction failed. Please contact admin.", "custom", 5000, { background: '#f8d7da', text: "#721c24"});
            dispatch(userloading(false));
        }).catch((error) =>{
            dispatch(userloading(false));
            notify.show(error.response.data.message, "custom", 5000, { background: '#f8d7da', text: "#721c24"});
        })
    };
}


export function verifyPRCode(url) {
    return (dispatch) => {
        dispatch(userloading(true));      
        axios.get(url,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            }
        }).then(response =>{
            if(response.status===200){
                if(response.data.canFollow){
                    let params = {
                        "following_user_id":response.data.following_user_id,
                         "user_id":localStorage.getItem("i_user")
                    }
                    dispatch(addPRCode(params))           
                }else{
                    notify.show(response.data.message, "custom", 5000, { background: '#f8d7da', text: "#721c24"});        
                }
                //dispatch(signuppage(5)); 
            }else
                notify.show("Failed to validate and attach PR Code. Please try again.", "custom", 5000, { background: '#f8d7da', text: "#721c24"});
            dispatch(userloading(false));
        }).catch((error) =>{
            dispatch(userloading(false));
            notify.show(error.response.data.message, "custom", 5000, { background: '#f8d7da', text: "#721c24"});
        })
    };
}


export function addPRCode(params) {
    return (dispatch) => {
        dispatch(signuploading(true));
        axios.post("/api/v1/signup/follow", params,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            }
        })
        .then((response) => {
            if(response.status==200){
                dispatch(signuppage(4));
                notify.show("PR Code added successfully", "custom", 2000, { background: '#d4edda', text: "#155724",borderColor: "#c3e6cb"});
            }else{
                notify.show("Failed to attach PR Code. Please try again.", "custom", 5000, { background: '#f8d7da', text: "#721c24"});
            }
            dispatch(signuploading(false));
        })
        .catch((error) => {
            dispatch(signuploading(false));
            notify.show(error.response.data.message, "custom", 5000, { background: '#f8d7da', text: "#721c24"});
        })
    };
}

export function resendOTP(url, email) {
    return (dispatch) => {
        dispatch(signuploading(true));
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': "*"
            },
            body: JSON.stringify({ email: email})
        })
            .then((response) => {
                dispatch(signuploading(false));
                notify.show("OTP resent successfully", "custom", 2000, { background: '#d4edda', text: "#155724",borderColor: "#c3e6cb"});
            })
            .catch((error) => {
                dispatch(signuploading(false));
                notify.show("something went wrong. please try again.", "custom", 5000, { background: '#f8d7da', text: "#721c24"});
            })
    };
}