import axios from 'axios';
import { notify } from 'react-notify-toast';
import createHistory from 'history/createHashHistory'
import userType from '../Actions/profile-setup'
export default createHistory()
const globalHistory = new createHistory();

export function setupEvent() {
    return (dispatch) => {
         dispatch(eventLoading(false));
        
    }
}
export function eventLoading(bool) {
    return {
        type: 'EVENT_LOADING',
        status: bool
    };
}
export function EventError(bool) {
    return {
        type: 'EVENT_ERROR',
        error: bool
    };
}

export function eventData(data) {
    return {
        type: 'EVENT_DATA',
        data
    };
}

export function events(data) {
    return {
        type: 'EVENTS',
        data
    };
}
export function saveEvent(url, headers, data, history) {
    return (dispatch) => {
        
        dispatch(eventLoading(true));
        axios.post(url, data, { headers: headers })
            .then(response => {
                
                notify.show("Event setup successfully", "custom", 2000, { background: '#d4edda', text: "#155724", borderColor: "#c3e6cb" });
                if(history !== null){
                    history.push("/dashboard");
                }
            })
            .catch(error => {
                dispatch(eventLoading(false));
                notify.show("failed to create Event", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
                
                history.push("/setup-event");
            });
    }

}

export function getEvent(url, headers, schema_name) {
    return (dispatch) => {

         dispatch(eventLoading(true));
                dispatch(getEventSchema(schema_name));
    }

}
export function getEventSchema(schema_name) {
    return (dispatch) => {
        axios.get("/api/v1/form-schema/" + schema_name)
            .then(response => {
                if (response.data.result.schema.length > 0) {
                    dispatch(eventData(response.data.result.schema));
                    dispatch(eventLoading(false));
                }
                if(response.data.result.schema.length === undefined){
                    dispatch(eventData([]));
                    dispatch(eventLoading(false));
                }
            })
            .catch(error => {
                dispatch(eventLoading(false));
                notify.show("something went wrong please reload page or login again.", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
            });
    }
}

export function getAllEvents(url, headers) {
    return (dispatch) => {
      dispatch(eventLoading(true));
        axios.get(url, headers)
            .then(res => {
                dispatch(events(res.data.result.events));
                dispatch(eventLoading(false));
            })
            .catch(error => {
                dispatch(eventLoading(false));
                notify.show("failed to fetch Events", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
                localStorage.removeItem("token");
            });
    }
}