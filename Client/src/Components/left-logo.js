import React from 'react';
import {Link} from 'react-router-dom'
const Logo = () => (
    <div className="main_left">
      <div className="logo_3 my-5">
        <Link to="#"><img src={require("../Utils/Assets/logo_3.png")} alt="logo"/></Link>
      </div>
    </div>
  );
  export default Logo;