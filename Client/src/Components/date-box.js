import React from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ReactTooltip from 'react-tooltip' 

const DateBox = (props) =>(
    <>
   <label><b>{props.attributes.placeholder}</b> 
   {props.attributes.info ? <span style={{marginLeft:"5px"}}><i className="fa fa-question-circle" data-tip data-for='global' style={{ fontSize: "1.2em" }}></i>
        <ReactTooltip className="toolTip" id='global' aria-haspopup='true' effect="solid" place="right">
        <p style={{ margin: 0, padding: 0 }}>{props.attributes.info}</p>
          {/* {paras.map(el => {
            return [<p style={{ margin: 0, padding: 0 }}>{el}</p>]
          })} */}
        </ReactTooltip></span> : ""}
        </label>
    <DatePicker 
        selected={props.attributes.value?new Date(props.attributes.value):""} 
        className={props.submitted && props.attributes.value===""?"form-control invalid":"form-control"}
        placeholderText="Click to select a date"
        onChange={(date) => props.IsUnderCustom ? 
            props.onCustomHandler(date.toString(), props.index, props.parentIndex) : 
            props.onChangeDateHandler(date.toString(), props.index, props.parentIndex)} 
     />
     </>
);
export default DateBox;