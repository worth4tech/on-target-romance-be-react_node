import React from 'react';
const TextBox = (props) =>(
    <>
    {/* <label>{props.attributes.placeholder}</label> */}
        <input 
            type={props.attributes.type} 
            className={props.attributes.className} 
            placeholder={props.attributes.placeholder} 
            value={props.attributes.value?props.attributes.value:""} 
            onChange={(e) => props.IsUnderCustom ? 
                props.onCustomHandler(e.target.value, props.index, props.parentIndex) : 
                props.onChangeTextHandler(e.target.value, props.index)} 
            required={props.attributes.validation} />
            </>
);
export default TextBox;