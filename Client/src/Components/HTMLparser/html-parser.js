import React, { Component } from 'react';
import TextBox from '../text-box';
import SelectBox from '../select-box';
import FileBox from '../file-box';
import DateBox from '../date-box';

let position=0
class HTMLParser extends Component {
  
  getFormElements = function (el, index, onChangeHandler, onCustomChangeHandler, onChangeFileHandler, onCheckboxChangeHandler, addMoreServiceHandler, removeServiceHandler, addMoreLinkHandler, onButtonClickHandler, childIndex, isSubmitted) {
    //console.log("this.state.formJSON", this.state.formJSON);
    el.tag === "title" || el.tag === "custom" ? position = 0 : position++;
    switch (el.tag) {
     
      case "blank":
          
        return(<div className={position % 2 === 0 ? "rght" : "lft"} style={{minHeight:"100px"}} key={index}>
          <div className="form-group"></div>
        </div>)

      case "input":
        console.log(el.className);
        
        return (
          <div className={el.width?el.width:position % 2 === 0 ? "rght" : "lft"} key={index}>
            <div className="form-group">
              <TextBox
                attributes={el}
                IsUnderCustom={false}
                onCustomHandler={onCustomChangeHandler}
                onChangeTextHandler={onChangeHandler}
                index={index}
                parentIndex={0}
              />
            </div>
          </div>
        )
      case "select":
        return (
          <div className={position % 2 === 0 ? "rght" : "lft"} key={index}>
            <div className="form-group">
              <SelectBox
                attributes={el}
                IsUnderCustom={true}
                onCustomHandler={onCustomChangeHandler}
                onChangeTextHandler={onChangeHandler}
                index={0}
                parentIndex={index}
              />

            </div>
          </div>
        )
      case "title":
        return (
          <div className="full_title" key={index}>
            {this.getTitleElement(el)}
          </div>
        )
      case "textarea":
        return (
          <div className="full_title" key={index}>
            <div className="form-group">
              {this.getTextAreaElement(el, index, 0, false, onChangeHandler, onCustomChangeHandler)}
            </div>
          </div>
        )
      case "file":
        return (
          <div className={position % 2 === 0 ? "rght" : "lft"} key={index}>
            <div className="form-group">
              <FileBox attributes={el} index={index} parentIndex={0} onFileHandler={onChangeFileHandler} />
            </div>
          </div>
        )
      case "radio":
        return (
          <div   key={index}>
            <div style={{marginTop:"0px"}}>
              <div className="in_form" style={{ border: "none",marginBottom:0 }}>
                {/* <div className="in_title_two">
                  
                </div> */}
                <div className="radiobuttons" style={{marginBottom:0,display:"inline-flex" }}>
                <span style={{marginRight:"10px"}}>{el.placeholder}</span>
                  {this.getRadioButtons(el, index, childIndex, false, onChangeHandler)}
                </div>
              </div>
            </div>
          </div>
        );

      case "checkbox":
        return (
          <div className="full_title" key={index}>
            <div className="form-group">
              <span style={{ color: "#1f1f1f", fontWeight: "600" }}>{el.placeholder}</span>
              {this.getCheckBoxes(el, index, 0, false, onCheckboxChangeHandler)}
            </div>
          </div>

        );

      case "custom":
        return (
          <div className="full_title" key={index}>
            <div className="form-group fam_gp">
              <div className="in_form row" style={{marginLeft:0}}>
                <div className="in_title"> <label>{el.title}:</label> </div>
                {this.getCustomComponent(el, index, onChangeHandler, onCustomChangeHandler, onChangeFileHandler, onCheckboxChangeHandler, addMoreServiceHandler, removeServiceHandler, addMoreLinkHandler, onButtonClickHandler)}
              </div>
            </div>
          </div>
        )
      case "date":
        return (
          <div className={position % 2 === 0 ? "rght" : "lft"} key={index}>
            <DateBox
              attributes={el}
              index={index}
              IsUnderCustom={false}
              parentIndex={index}
              onCustomHandler={onCustomChangeHandler}
              onChangeDateHandler={onChangeHandler}
              submitted={isSubmitted}
            />

          </div>
        )
      case "switch":
        return (
          <div className="left" key={index}>
            <div className="form-group fam_gp">
              <div className="in_form" style={{ border: "none" }}>
                <div className="toggle_switch">
                  <span className="yes_no">{el.valueOn}</span>
                  <label className="switch"><input type="checkbox" checked={el.value === el.valueOff} name="myevents" onChange={() => onChangeHandler(el.name, index, undefined)} /><span className="slider round"></span></label>
                  <span className="yes_no">{el.valueOff}</span>
                </div>
              </div>
            </div>
          </div>




        );
      case "customdate":
        return (
          <div className="full_title" key={index}>
            <div className="form-group fam_gp">
              <div className="in_form">
                <div className="in_title"> <span>{el.placeholder}:</span> </div>
                {this.getCustomComponent(el, index, onChangeHandler, onCustomChangeHandler, onChangeFileHandler, onCheckboxChangeHandler, addMoreServiceHandler, removeServiceHandler, addMoreLinkHandler, onButtonClickHandler)}
              </div>
            </div>
          </div>
        )
      default:
        return (<div></div>)
      case "button":
        return (
          <div className={position % 2 === 0 ? "rght" : "lft"} key={index}>
            <div className="form-group">

              <input
                type="button"
                className={el.className}
                value={el.value}
                onClick={() => {
                  onButtonClickHandler(el)
                }}
              />
            </div></div>
        )

    }
    

  }

  getCustomComponent = function (attributes, parentIndex, onChangeHandler, onCustomChangeHandler, onChangeFileHandler, onCheckboxChangeHandler, addMoreServiceHandler, removeServiceHandler, addMoreLinkHandler, onButtonClickHandler,isSubmitted) {
    var position = 0;
    position++;
    return attributes.children.map((el, index) => {
      el.className = "form-control";
      switch (el.tag) {

        // case "blank":
        // return(<div className={position % 2 === 0 ? "rght" : "lft"} key={index}></div>)

        case "input":
          return <div className={el.width?el.width:position % 2 === 0 ? "rght" : "lft"} key={index}><React.Fragment key={index + "_" + parentIndex}>
            <TextBox
              attributes={el}
              IsUnderCustom={true}
              onCustomHandler={onCustomChangeHandler}
              onChangeTextHandler={onChangeHandler}
              index={index}
              parentIndex={parentIndex}
            />
          </React.Fragment></div>
        case "select":
          return (
            <div className={position % 2 === 0 ? "rght" : "lft"}><React.Fragment key={index + "_" + parentIndex}>
              <div  key={index + "_" + parentIndex}>
                <SelectBox
                  attributes={el}
                  IsUnderCustom={true}
                  onCustomHandler={onCustomChangeHandler}
                  onChangeTextHandler={onChangeHandler}
                  index={index}
                  parentIndex={parentIndex}
                />
              </div>
            </React.Fragment></div>
          )
        case "title":
          return (
            <div className="full_title" key={index}>
              {this.getTitleElement(el)}
            </div>
          )

        case "date":
          return (

            <div className={position % 2 === 0 ? "rght" : "lft"} key={index}>
              <div className="form-group">
                <DateBox
                  attributes={el}
                  index={index}
                  IsUnderCustom={true}
                  parentIndex={parentIndex}
                  onCustomHandler={onCustomChangeHandler}
                  onChangeDateHandler={onChangeHandler} 
                  submitted={isSubmitted}/>
                    
              </div>
            </div>
          )
        case "service":
          return (

            <div className={position % 2 === 0 ? "rght" : "lft"} key={index}>
              <div className="form-group">
                <TextBox
                  attributes={el}
                  IsUnderCustom={true}
                  onCustomHandler={onCustomChangeHandler}
                  onChangeTextHandler={onChangeHandler}
                  index={index}
                  parentIndex={parentIndex}
                />
              </div>
            </div>
          );
        case "switch":
          return (
            <div className="toggle_switch">
              <span className="yes_no">{el.valueOn}</span>
              <label className="switch"><input type="checkbox" name="myevents" onChange={() => onCustomChangeHandler(el.name, index, parentIndex)} /><span class="slider round"></span></label>
              <span className="yes_no">{el.valueOff}</span>
            </div>




          );
        case "add":
          return (
            <div className="ft_text" key={"plus" + index}><span >{el.placeholder} <img src={require("../../Utils/Assets/plus.png")} alt="add" onClick={() => { el.type === "service" ? addMoreServiceHandler(parentIndex, index) : addMoreLinkHandler(parentIndex, index) }} className="addMore" /> </span></div>
          )
        case "minus":
          return (
            <div className="ft_text" key={"plus" + index}><span >{el.placeholder} <img src={require("../../Utils/Assets/minus.png")} alt="add" onClick={() => { el.type === "service" ? removeServiceHandler(parentIndex, index) : removeServiceHandler(parentIndex, index) }} className="addMore" /> </span></div>
          )
        default:
          return (this.getFormElements(el, parentIndex, onChangeHandler, onCustomChangeHandler, onChangeFileHandler, onCheckboxChangeHandler, addMoreServiceHandler, removeServiceHandler, addMoreLinkHandler, onButtonClickHandler, index))

      }

    })
  }

  getCheckBoxes(attributes, parentIndex, index, IsUnderCustom, onCheckboxChangeHandler) {
    return attributes.buttons.map((el, i) => {
      return (
        <div className="custom-control custom-checkbox white col-md-4 " style={{ float: "left" }}>
          <input type="checkbox" checked={attributes.value !== undefined && attributes.value.includes(el.name)}
            onChange={() => onCheckboxChangeHandler(el.name, parentIndex)} id={"check_" + parentIndex + "_" + i} style={{ display: "none" }} />
          <label htmlFor={"check_" + parentIndex + "_" + i} className="checkbox_label_black">
            <div className="mark"></div>
          </label><p>{el.name}</p>
        </div>

      )
    })
  }

  getRadioButtons(attributes, parentIndex, index, IsUnderCustom, onChangeHandler) {
    return attributes.buttons.map((el, i) => {
      return (
        <div className="rdio rdio-primary radio-inline">
          <input name={attributes.name} id={"radio_" + parentIndex + "_" + i} type="radio" className="author-radio" value="1" checked={attributes.value === el.name}
            onChange={() => onChangeHandler(el.name, parentIndex, index)} />
          <label key={el.name} htmlFor={"radio_" + parentIndex + "_" + i}>{el.name}</label>
        </div>
      )
    });

  }

  getTextAreaElement(attributes, index, parentIndex, IsUnderCustom, onChangeHandler, onCustomChangeHandler) {
    return (
      <textarea type={attributes.type} value={attributes.value ? attributes.value : ""} className={attributes.className} placeholder={attributes.placeholder} onChange={(e) => IsUnderCustom ? onCustomChangeHandler(e.target.value, index, parentIndex) : onChangeHandler(e.target.value, index)} required={attributes.validation} ></textarea>
    );
  }

  getTitleElement(attributes) {
    return (<label>{attributes.title}:</label>)
  }

  componentDidMount() {
    position=0
  }
  

}
export default HTMLParser;