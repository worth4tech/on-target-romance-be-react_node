import React from 'react';
const FileBox = (props) =>{
    if(props.attributes.value!==""){
        let path = props.attributes.value;
        return(
          <img src= {path} height="60"  alt="profile"/>
        );
      }

      return (
        <>
          <input type={props.attributes.type} 
                onChange={(e) => 
                    props.onFileHandler(e.target.files[0], props.attributes.size)}
                    data-max-size={props.attributes.size}
                    required={props.attributes.validation} />
          
          <div className="upload_text" style={{ marginRight: 30 }}>
            <span style={{ color: "#1f1f1f", fontWeight: "600" }}>Upload Photo *</span>
            <p style={{ color: "#8a8a8a", fontWeight: "600" }}>(max file size 3MB)</p>
          </div>
        </>
  
      );

            };
export default FileBox;