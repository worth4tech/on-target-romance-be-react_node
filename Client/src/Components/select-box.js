import React from 'react';
const SelectBox = (props) =>(
        <select 
            className={props.attributes.className} 
            placeholder={props.attributes.placeholder} 
            value={props.attributes.value?props.attributes.value:""} 
            onChange={(e) => props.IsUnderCustom ? 
                props.onCustomHandler(e.target.value, props.index, props.parentIndex) : 
                props.onChangeTextHandler(e.target.value, props.index)} 
            required={props.attributes.validation} >
             <option value="" key="">{props.attributes.placeholder}</option>
            <Options options={props.attributes.options}/>
             </select>
);

const Options = (props) =>{ 
        return props.options.map((el, index) => {
          return <option value={el.value} key={el.value + "_" + index}>{el.text}</option>
        })
      }
export default SelectBox;