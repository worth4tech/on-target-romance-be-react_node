import React, { Component, Suspense } from 'react';
import { css } from '@emotion/core';
import { GridLoader } from 'react-spinners';
import { connect } from 'react-redux';
import {
  HashRouter,Redirect,
  Route,Switch 
} from 'react-router-dom'
import { authenticate } from './Actions/index';
import Landing from './Pages/Landing/landing';
import Login from './Pages/Login/login';
import SignUp from './Pages/SignUp/signup';
import Event from './Pages/Event/event';
import Dashboard  from './Pages/Profile/dashboard';
import UsersListView  from './Pages/usersList/usersList';
import './App.css';

const override = css`
    position:fixed;
    margin-top: 15%;
    left: 45%;
`;

class App extends Component {
  constructor(props){
    super(props);
    this.state={user:""}
  }


  render() {
        if(!localStorage.getItem("token")){
          return [
            <HashRouter>
              <Switch>
                <Route exact path="/" component={Landing} />
                <Route exact path="/log-in" component={Login} />
                <Route exact path="/sign-up" component={SignUp} />
                <Route path="*" render={()=><Redirect to="/" push/>} />
              </Switch>
            </HashRouter>,
            this.props.status || this.props.loading ?
              <div className='loader-container'>     
                      <GridLoader
                          css={override}
                          sizeUnit={"px"}
                          size={30}
                          color={'#e20f0b'}
                          loading={true}
                          />
                      
              </div>:"" 
          ];
        }else{
          return[
            this.props.status || this.props.loading || this.props.eventLoad?
          <div className='loader-container'>     
                  <GridLoader
                      css={override}
                      sizeUnit={"px"}
                      size={30}
                      color={'#e20f0b'}
                      loading={true}
                      />
          </div>:"",
          <HashRouter>
            <div>
              <Route exact path="/" render={()=><Redirect to="/user" push/>} />
              <Route exact path="/log-in" render={()=><Redirect to="/user" push/>} />
              <Route exact path="/sign-up" component={SignUp} />
              <Route exact path="/setup-event" component={Event} />
              <Route exact path="/dashboard" component={Dashboard} />
              <Route  path="/user" component={Home} />
              <Route exact path="/user-list" component={UsersListView} />
            </div>
          </HashRouter>
           
          ]
        }
        
    
  }
};

const LazyHome = React.lazy(() => import('./Pages/Home/home'));
const Home = () => (
  <Suspense fallback={<div></div>}>
    <LazyHome />
  </Suspense>
);
 
const mapStateToProps = (state) => {
  return {
    status: state.userloading,
    error: state.usererror,
    token:state.userlists,
    loading: state.signuploading,
    eventLoad:state.eventLoading
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: (url) => dispatch(authenticate(url))
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(App);