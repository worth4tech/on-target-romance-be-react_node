import {combineReducers} from 'redux';
import {userlists,userloading,usererror,signuppage,signuploading} from './users';
import {profileError,profileLoading,profileData, userType, userListData} from './profile';
import { eventError,eventLoading,eventData,events } from './event'
export default combineReducers({
    userlists,
    userloading,
    usererror,
    signuppage,signuploading,
    profileError,profileLoading,profileData, userType, userListData,
    eventError,eventLoading,eventData,events
});