export function profileLoading(state=false,action){
    switch(action.type){
        case'PROFILE_LOADING':
            return action.status;
        
        default:
            return state;
    }
}

export function profileError(state=true,action){
    switch(action.type){
        case'PROFILE_ERROR':
            return action.error;
        
        default:
            return state;
    }
}

export function profileData(state="",action){
    switch(action.type){
        case'PROFILE_DATA':
            return action.data;
        default:
            return state;
    }
}

export function userType(state="",action){
    switch(action.type){
        case'USER_TYPE':
            return action.user_type;
        default:
            return state;
    }
}

export function userListData(state="",action){
    switch(action.type){
        case'USERLIST_DATA':
            return action.data;
        default:
            return state;
    }
}
