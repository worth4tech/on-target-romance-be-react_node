export function userlists(state="",action){
    
    switch(action.type){
        case'USERS_LOADED':
            return action.token;
        
        default:
            return state;
    }
}

export function userloading(state=false,action){
    switch(action.type){
        case'USERS_LOADING':
            return action.status;
        
        default:
            return state;
    }
}

export function signuploading(state=false,action){
    switch(action.type){
        case'SIGNUP_LOADING':
            return action.signup_status;
        
        default:
            return state;
    }
}

export function usererror(state=true,action){
    switch(action.type){
        case'USERS_ERROR':
            return action.error;
        
        default:
            return state;
    }
}

export function signuppage(state=0,action){
    switch(action.type){
        case'SIGNUP_PAGE':
            return action.page;
        
        default:
            return state;
    }
}