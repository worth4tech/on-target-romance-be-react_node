export function eventLoading(state=false,action){
    switch(action.type){
        case'EVENT_LOADING':
            return action.status;
        
        default:
            return state;
    }
}

export function eventError(state=true,action){
    switch(action.type){
        case'EVENT_ERROR':
            return action.error;
        
        default:
            return state;
    }
}

export function eventData(state="",action){
    switch(action.type){
        case'EVENT_DATA':
            return action.data;
        default:
            return state;
    }
}

export function events(state="",action){
    switch(action.type){
        case'EVENTS':
            return action.data;
        default:
            return state;
    }
}
