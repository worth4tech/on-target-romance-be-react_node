import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { authenticate, signuppage } from '../../Actions/index';
import { css } from '@emotion/core';
import { GridLoader, BounceLoader } from 'react-spinners';
import './landing.css';

const override = css`
    position:relative;
`;

class Landing extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: "",
            password: "",
            isLoading: false,
            selectedLoader: 0
        }
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
    }

    handleChangeEmail(e) {
        this.setState({ email: e.target.value });
    }
    handleChangePassword(e) {
        this.setState({ password: e.target.value });
    }
    handleLoginSubmit(e, history) {
        e.preventDefault();
        this.props.fetchData("/api/v1/login", this.state.email, this.state.password, history);
    }

    render() {
        return (
            <React.Fragment>
                {/* <Logo /> */}

                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <div className="navbar-collapse">
                        <ul className="navbar-nav mr-auto">
                        </ul>
                        <Route render={({ history }) => (
                            <form className="form-inline my-2 my-lg-0" onSubmit={(e) => { this.handleLoginSubmit(e, history) }}>
                                <input className="form-control form_cust_hed mr-sm-2" type="email" placeholder="Username" aria-label="" onChange={this.handleChangeEmail} required></input>
                                <input className="form-control form_cust_hed mr-sm-2" type="password" placeholder="Password" aria-label="" onChange={this.handleChangePassword} required />
                                <button className="btn btn_h_login my-2 my-sm-0" type="submit">Login</button>
                            </form>
                        )}></Route>
                    </div>
                </nav>

                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="hedv_text">
                                <span style={{ color: "#1f1f1f" }}>Learn more about membership options with our short videos</span>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 col-sm-2">
                            <img className="left-logo" src={require("../../Utils/Assets/logo_2.png")} alt="" />
                        </div>
                        <div className="col-12 col-sm-4">
                            <VideoFrames />
                        </div>
                        <div className="col-12 col-sm-4">
                            <VideoFrames />
                        </div>
                        <div className="col-12 col-sm-2"></div>
                    </div>
                    <div className="row">
                        <div className="col-12 col-sm-2">
                        </div>
                        <div className="col-12 col-sm-4">
                            <VideoFrames />
                        </div>
                        <div className="col-12 col-sm-4">
                            <VideoFrames />
                        </div>
                        <div className="col-12 col-sm-2"></div>
                    </div>
                </div>
                <Footer />
                
                
            </React.Fragment>
        );
    }

    componentDidMount() {
        this.props.resetSignupPage();
        console.log('componentDidMount');
    }
}


//{======= Logo Component ========}
const Logo = () => (
    <div className="container">
        <div className="row">
            <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div className="logo logo_two my-2"> <Link to="#"><img src={require("../../Utils/Assets/logo_2.png")} alt="" /></Link> </div>
            </div>
        </div>
    </div>
);

//{======= VideoFrames Component ========}
const VideoFrames = () => (
    <div className="video">
        <iframe width="100%" src="https://www.youtube.com/embed/yodw5jIOBt4" frameBorder="0" title="video 1" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
    </div>
);


//{======= Footter Component ========} 
const Footer = () => (
    <footer>
        <div className="container">
            <div className="row">
                <div className="col-12 col-sm-6 mx-auto">
                    <div className="footer_text"> <span style={{ color: "#fff" }}>Welcome to <strong>On Target Romance,</strong></span>
                        <p style={{ color: "#fff" }}> a calendar with the primary purpose of helping romance readers, authors and book industry professionals organize their
            reading and share information in order to stay up-to-date on book-related events.</p>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-12 col-sm-10 col-md-8 col-lg-6 mx-auto">
                    <div className="row">
                        <div className="col-sm-6 col-md-6 col-lg-6 mx-auto">
                            <div>
                                <Route render={({ history }) => (<button className="btn btn-lg btn-block btn_login pull-right" type="button" style={{ width: '80%' }} onClick={() => { history.push("/sign-up") }}>Join</button>)}></Route>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-6 mx-auto">
                            <div>
                                <Route render={({ history }) => (<button className="btn btn-lg btn-block  btn_login" type="button" style={{ width: '80%' }} onClick={() => { history.push("/log-in") }}>Login</button>)}></Route>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </footer>
);

const mapStateToProps = (state) => {
    return {
        users: state.userlists,
        status: state.userloading
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url, login, password, history) => dispatch(authenticate(url, login, password, history)),
        resetSignupPage: () => dispatch(signuppage(0))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Landing);