import React, { Component } from 'react';
import { notify } from 'react-notify-toast';
import { connect } from 'react-redux';
import { Link, Redirect, Route } from 'react-router-dom';
import { HashLoader } from 'react-spinners';
import { getAllEvents, setupEvent } from '../../Actions/event-setup';
import Logo from '../../Components/left-logo';
import EventsCalendarView from '../dashboard/eventsCalendarView';
import EventsListView from '../dashboard/eventsListView';
import './../usersList/usersList.css';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formJSON: [],
      event: null,
      myevents: true,
      isCalenderView: true,
      showMenu: false,
    }
    this.props.setupEvent();
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onCheckboxChangeHandler = this.onCheckboxChangeHandler.bind(this);
    this.onCustomChangeHandler = this.onCustomChangeHandler.bind(this);
    this.onChangeFileHandler = this.onChangeFileHandler.bind(this);
    this.onSubmitFormHandler = this.onSubmitFormHandler.bind(this);
    this.getCustomElementObject = this.getCustomElementObject.bind(this);
    this.addMoreLinkHandler = this.addMoreLinkHandler.bind(this);
    this.addMoreServiceHandler = this.addMoreServiceHandler.bind(this);
    this.changeEventsView = this.changeEventsView.bind(this);
    this.hideDropDown = this.hideDropDown.bind(this);
    this.showDropDown = this.showDropDown.bind(this);
  }


  //======================= On Change Handlers ================
  showDropDown() {
    this.setState({
      showMenu: !this.state.showMenu
    });
  }

  hideDropDown() {
    this.setState({
      showMenu: false
    });
  }

  handleMyEvents(event) {
    this.setState({
      myevents: event.target.checked
    });
  }
  onChangeHandler(value, index) {
    let data = this.state.formJSON;
    data[index].value = value;
    this.setState({ formJSON: data });
  }
  onCheckboxChangeHandler(value, parentIndex) {
    let data = this.state.formJSON;
    if (data[parentIndex].value.includes(value)) {
      data[parentIndex].value = data[parentIndex].value.replace(value, "");
    }
    else {
      data[parentIndex].value += data[parentIndex].value.concat(value);
    }

    // if(data[parentIndex].value.contains(value))
    // {
    //   data[parentIndex].value = data[parentIndex].value.replace(data[parentIndex].value).trim(",");
    // }
    // else{
    //   data[parentIndex].value += "," + value;
    // } 
    this.setState({ formJSON: data });
  }

  onCustomChangeHandler(value, index, parentIndex) {
    let data = this.state.formJSON;
    data[parentIndex].children[index].value = value;
    this.setState({ formJSON: data });
  }

  onChangeFileHandler(file, acceptedSize) {
    if (parseInt(acceptedSize) * 1000 <= parseInt(file.size)) {
      notify.show("File size is bigger than max size. Profile will be saved without Image.", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
      return;
    }
    this.setState({ profile: file });
  }

  onSubmitFormHandler(e, history) {
    e.preventDefault();
    history.push("/setup-event");
  }

  addMoreLinkHandler(parentIndex, index) {
    let data = this.state.formJSON;
    data[parentIndex].children.push(data[parentIndex].children[index]);
    let el = {
      "tag": "input",
      "type": "text",
      "className": "form-control in_form_control",
      "placeholder": "link",
      "value": "",
      "validation": false,
      "name": ""
    };
    data[parentIndex].children[index] = el;
    data[parentIndex].children[index].name = data[parentIndex].linkname + (parseInt(data[parentIndex].links) + 1);
    data[parentIndex].links = parseInt(data[parentIndex].links) + 1;

    this.setState({ formJSON: data });
  }

  addMoreServiceHandler(parentIndex, index) {
    let data = this.state.formJSON;
    //data[parentIndex].children.push(data[parentIndex].children[index]);
    let el = data[parentIndex].children[index];
    let el1 = { "tag": "date", "value": "", "name": "" };
    let el2 = { "tag": "service", "name": "", "type": "text", "value": "", "className": "form-control", "validation": "false", "placeholder": "Service" };

    data[parentIndex].children[index] = el1;
    data[parentIndex].children[index].name = "date_" + (parseInt(data[parentIndex].service) + 1);
    data[parentIndex].children.push(el2);
    data[parentIndex].children[index + 1].name = "service_" + (parseInt(data[parentIndex].service) + 1);
    data[parentIndex].service = parseInt(data[parentIndex].service) + 1;
    data[parentIndex].children.push(el);
    this.setState({ formJSON: data });
  }

  changeEventsView(value) {
    this.setState({
      isCalenderView: value
    });
  }
  //======================= On Change Handlers ENDS ================


  //======================= Common functions ====================

  getJSONFormData() {
    const formData = new FormData();
    this.state.formJSON.map(el => {
      if (el.name) {
        if (el.tag === "file")
          formData.append(el.name, this.state.profile);
        else if (el.tag === "custom") {

          let data = this.getCustomElementObject(el.children);

          formData.append(el.name, data);
        }
        else
          formData.append(el.name, el.value);
      }
    });
    return formData;
  }

  getCustomElementObject(children) {
    let data = {};
    children.map(el => {
      if (el.name) {
        data[el.name] = el.value;
      }
    });
    return JSON.stringify(data);
  }

  //======================= Common functions ENDS ====================

  //======================= Custom DOM Providers ================

  //======================= Custom DOM Providers ENDS ================


  //======================= Custom Component Providers ================



  //======================= Custom Component Providers ENDS ================




  //===================== RENDER ==========================
  render() {
    return (
      <>
        <div style={{ backgroundColor: "#1f1f1f" }}>
          <div className="container-fluid">
            <div className="row">
              <Logo />
              <div className="main_right author_profile" style={{ minHeight: 1000 + 'px' }}>
                <div className={this.state.myevents ? "navigation bg-light" : "navigation bg-dark"}>
                  <nav className={this.state.myevents ? "navbar navbar-expand-lg navbar-light bg-light" : "navbar navbar-expand-lg navbar-light bg-dark"}>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" >
                      <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                          <a className="nav-link" href="#">Home</a>
                        </li>
                        <li className="nav-item active">
                          <a className="nav-link" href="#">Calendar</a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="#">Event filter</a>
                        </li>
                        <li className="nav-item">
                          <Link className="nav-link" to={'/user-list'}>LISTS</Link>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="#">VENDORS</a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="#">daily task</a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="#">Messages</a>
                        </li>
                      </ul>
                    </div>
                  </nav>
                  <div className="noti_setting">
                    <a className="" href="#">
                      <em className="fa fa-2x fa-bell"
                        style={this.state.myevents ? { color: "#202020", fontSize: "1.7em", marginTop: 3, marginRight: 10 } : { color: "white", fontSize: "1.7em", marginTop: 3, marginRight: 10 }}>
                        <div>
                          <span className="badge custom-badge">10</span>
                        </div>
                      </em>
                    </a>
                    <a style={{ marginRight: "40px" }} className="dropdown">
                      {/* <img onClick={() => this.showDropDown()} src={require("../../Utils/Assets/setting_icon.png")} alt="setting" /> */}
                      <em className="fa fa-2x fa-cog" style={this.state.myevents ? { color: "#202020" } : { color: "white" }} onClick={() => this.showDropDown()}></em>
                      <div className={this.state.showMenu ? "dropdown-content show" : "dropdown-content"}>
                        <a href="/"><button className="btn btn-link" style={{ color: "black" }} >Profile</button></a>
                        <Link to={'/user-list'}><button className="btn btn-link" style={{ color: "black" }} >Users Lists</button></Link>
                        <a href="/"><button className="btn btn-link" style={{ color: "black" }} onClick={() => { localStorage.removeItem("token") }}>Log Out</button></a>
                      </div>
                    </a>
                  </div>
                </div>

                <div className="filter_row">
                  <div className="row">
                    <div className="col-sm-12">
                      <div className="filter_options">
                        <div className="row">
                          <div className="col-sm-12">
                            <div className="toggle_switch">
                              <span className="yes_no">Dark</span>
                              <label className="switch">
                                <input type="checkbox" name="myevents" onChange={this.handleMyEvents.bind(this)} checked={this.state.myevents} />
                                <span className="slider round"></span>
                              </label>
                              <span className="yes_no">Light</span>
                            </div>
                            <div className="sport">
                              <a href="#">
                                <span>Sort</span>
                                <img src={require("../../Utils/Assets/sort_icon.png")} alt="sort-icon" />
                              </a>
                            </div>
                            <div className="new_filter">
                              <a href="#">
                                <img src={require("../../Utils/Assets/cup_icon.png")} alt="cup-icon" />
                              </a>
                            </div>
                            <div className="search_box">
                              <form>
                                <input type="text" placeholder="Search" name="search" />
                                <button type="submit"><img src={require("../../Utils/Assets/search_icon.png")} alt="search-icon" /></button>
                              </form>
                            </div>
                            <div className="calender_list">
                              <a className="calender cursor-pointer" onClick={() => this.changeEventsView(true)}>Calendar View</a><span>|</span><a className="list cursor-pointer" onClick={() => this.changeEventsView(false)}>List View</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={this.state.myevents ? "calendar-container" : "calendar-container calendar-container-dark"}>
                  <div className="row">
                    <div className="col-sm-12">
                      {this.props.eventsData ? this.state.isCalenderView ?
                        <EventsCalendarView eventsData={this.props.eventsData} isTheameLight={this.state.myevents} />
                        : <EventsListView eventsData={this.props.eventsData} isTheameLight={this.state.myevents} />
                        : null
                      }
                    </div>
                  </div>
                </div>
                <Route render={({ history }) => (
                  <div>
                    <form className="form_custom" onSubmit={(e) => (this.onSubmitFormHandler(e, history))}>
                      <div className="form-group">
                        <button type="submit" className="btn btn_save_bt" style={{ width: "13%", marginRight: "2%" }}>Setup Event</button>
                      </div>
                    </form>
                    <div className="color_guide">
                      <div className="row">
                        <div className="col-sm-4">
                          <sapn className="color_box">
                          </sapn>
                          <p className="indicator_name">BOOK RELEASE</p>
                        </div>
                        <div className="col-sm-4">
                          <sapn className="color_box">
                          </sapn>
                          <p className="indicator_name">GIVEAWAY</p>
                        </div>
                        <div className="col-sm-4">
                          <sapn className="color_box">
                          </sapn>
                          <p className="indicator_name">COVER REVEAL PROMO</p>
                        </div>
                        <div className="col-sm-4">
                          <sapn className="color_box">
                          </sapn>
                          <p className="indicator_name">RELEASE PARTY</p>
                        </div>
                        <div className="col-sm-4">
                          <sapn className="color_box">
                          </sapn>
                          <p className="indicator_name">COVER REVEAL</p>
                        </div>
                        <div className="col-sm-4">
                          <sapn className="color_box">
                          </sapn>
                          <p className="indicator_name">BOOK SIGNING</p>
                        </div>
                        <div className="col-sm-4">
                          <sapn className="color_box">
                          </sapn>
                          <p className="indicator_name">ARC SIGN UP</p>
                        </div>
                      </div>
                    </div>
                  </div>
                )}></Route>
              </div>
            </div>
          </div>
        </div >
      </>);

  }

  componentDidMount() {
    this.props.getAllEvents();
  }
  componentWillReceiveProps() {
    this.setState({ formJSON: this.props.formJSON });

  }

}
const mapStateToProps = (state) => {
  return {
    loading: state.eventLoading,
    eventsData: state.events,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    setupEvent: () => dispatch(setupEvent()),
    getAllEvents: () => dispatch(getAllEvents("/api/v1/events", { headers: { 'x-authorization': localStorage.getItem("token") } })),
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

