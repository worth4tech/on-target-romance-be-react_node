import 'ag-grid-community';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import 'ag-grid-community/dist/styles/ag-theme-dark.css';
import { AgGridReact } from 'ag-grid-react';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setupEvent } from '../../Actions/event-setup';
import EventModal from './eventModal';

class EventsListView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            eventData: {},
            columnDefs: [
                {
                    headerName: "ID", field: "id", hide: true
                },
                {
                    headerName: "CATEGORY", field: "event_type", sortable: true, filter: true, resizable: true,
                    cellRenderer: function (event_type) {
                        var backgroundColor;
                        if (event_type.value === "Book Release") {
                            backgroundColor = "#009933"
                        }
                        if (event_type.value === "Giveaway") {
                            backgroundColor = "#d24dff"
                        }
                        if (event_type.value === "Cover Reveal Promo") {
                            backgroundColor = "#ff33cc"
                        }
                        if (event_type.value === "Release Party") {
                            backgroundColor = "Aqua"
                        }
                        if (event_type.value === "Cover Reveal") {
                            backgroundColor = "#ff6600"
                        }
                        if (event_type.value === "Book Signing") {
                            backgroundColor = "Yellow"
                        }
                        if (event_type.value === "Arc Signup") {
                            backgroundColor = "#0066cc"
                        }
                        return '<span style="height: 13px;width: 13px; background-color:' + backgroundColor
                            + '; border-radius: 50%; display: inline-block"></span> ' + event_type.value;
                    },
                },
                {
                    headerName: "TITLE", field: "title", sortable: true, filter: true, resizable: true
                },
                {
                    headerName: "AUTHOR NAME", field: "author_first_name", sortable: true, filter: true, resizable: true
                },
                {
                    headerName: "START DATE", field: "start", sortable: true, filter: true, resizable: true
                },
                {
                    headerName: "END DATE", field: "end", sortable: true, filter: true, resizable: true
                },
                {
                    headerName: "FAVORITE", field: "", sortable: true, filter: true, resizable: true,
                    cellRenderer: function () {
                        return '<i class="fa fa-star" style="font-size:20px;color:#ffff4d"></i>';
                    }
                }
            ],
            defaultColDef: {
                editable: true,
                sortable: true,
                resizable: true,
                filter: true
            },
            paginationPageSize: 10,
            paginationNumberFormatter: function (params) {
                return "" + params.value.toLocaleString() + "";
            },
            displayedRows: 0,
        }
        this.hideEvent = this.hideEvent.bind(this);
    }

    showEvent(e) {
        this.setState({
            eventData: e.data,
            modal: true
        });
    }

    hideEvent() {
        this.setState({
            modal: false,
            eventData: {},
        });
    }

    onGridReady = params => {
        this.gridApi = params.api;
        this.columnApi = params.columnApi;
        this.gridApi.sizeColumnsToFit();
        this.gridApi.paginationGoToPage(this.props.current);
        this.gridApi.setDomLayout("autoHeight");
    }

    refreshGrid() {
        this.props.getCases();
    }

    onPageSizeChanged(newPageSize) {
        var value = document.getElementById("page-size").value;
        this.gridApi.paginationSetPageSize(Number(value));
    }

    onFilterChanged(e) {
        var value = document.getElementById("search").value;
        this.gridApi.setQuickFilter(value);
        this.setState({ displayedRows: this.gridApi.getDisplayedRowCount() });
    }

    //===================== RENDER ==========================
    render() {
        return (
            <>
                <div className={this.props.isTheameLight ? "ag-theme-balham" : "ag-theme-dark home-ag"} style={{}}>
                    <AgGridReact
                        onGridReady={this.onGridReady}
                        columnDefs={this.state.columnDefs}
                        rowData={this.props.eventsData}
                        pagination={true}
                        reactNext={true}
                        animateRows
                        onCellClicked={this.showEvent.bind(this)}
                        paginationPageSize={this.state.paginationPageSize}
                        paginationNumberFormatter={this.state.paginationNumberFormatter}
                    />
                    <div className="test-header" style={{ float: "right" }}>
                        Page Size:
                         <select onChange={this.onPageSizeChanged.bind(this)} id="page-size" defaultValue="10">
                            <option value="10" >10</option>
                            <option value="100">100</option>
                            <option value="500">500</option>
                            <option value="1000">1000</option>
                            <option value="1000">1000</option>
                        </select>
                    </div>
                </div>
                {this.state.modal ?
                    <EventModal isOpen={this.state.modal} eventData={this.state.eventData} toggle={this.hideEvent} />
                    : null}
            </>);

    }

    componentDidMount() {
        if (this.props.eventsData !== '') {
            this.props.eventsData.forEach(function (event) {
                event.author_first_name = event.author_first_name+ " " + event.author_last_name
                // event.title = event.event_details_with_values && event.event_details_with_values.title
                // event.start = event.event_details_with_values && event.event_details_with_values.start
                // event.end = event.event_details_with_values && event.event_details_with_values.end
                // event.event_type = event.event_details_with_values && event.event_details_with_values.event_title
            });
        }
    }
}
const mapStateToProps = (state) => {
    return {
        loading: state.eventLoading
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setupEvent: () => dispatch(setupEvent()),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(EventsListView);

