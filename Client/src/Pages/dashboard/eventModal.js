import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import './dashboard.css'
import EventHTMLViewParser from './eventHtmlViewParser'
class EventModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            eventKeyData: [],
            title:""
        }
        this.htmlParser = new EventHTMLViewParser();
    }

    //===================== RENDER ==========================
    render() {
        const self = this;
        const objImg = this.props.eventData.event_details_with_values.find((_d) => _d.image);
        const pathImg = objImg ? objImg.image : require("../../Utils/Assets/book_release.jpg")
        return (
            <>
                <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} id="myModal">
                    <div className="model_cover">
                        <ModalHeader className="eventModal" style={{ 
                            backgroundImage: "url(" + pathImg + ")",
                            backgroundSize: "contain",
                            backgroundRepeat: "no-repeat",
                            height: "250px",
                            width: "100%",
                            backgroundPositionX: "center"
                        }}  toggle={this.props.toggle}>
                            {this.props.eventData ?
                                 <h3 style={{ paddingTop: "185px", textAlign: "center" }}>{this.state.title}</h3> : null}
                        </ModalHeader>

                        <ModalBody className="event_body">
                            <ul>
                                {this.state.eventKeyData.map((data) => {
                                     let key = Object.keys(data)[0];
                                    let value =  Object.values(data); 
                                   
                                    if (key && ( key.toLowerCase() === "undefined" || key.toLowerCase() === "end" ||key.toLowerCase() === "event_title" || key.toLowerCase() === "image" || key.toLowerCase() === "book-cover")) {
                                        return
                                    }
                                    if(Object.keys(data).length !== 0){
                                        if( value[0] && Array.isArray(value[0]) && value[0].length){
                                            return (<span><li><span><b>{key}</b></span></li><ul>
                                                {value[0].length && value[0].map((sub) => {
                                                  return sub.length>0 && sub.map((subObj) =>{
                                                   return (<li><span>{Object.keys(subObj)[0]}</span>:
                                                   <span> <b>{Object.values(subObj)[0]}</b></span></li>)
                                                   })
                                               })} 
                                           </ul></span>
                                       )
                                        }else if(!Array.isArray(value[0]) && value[0]!=="") {
                                            return( 
                                                <li><span>{key}</span>:
                                                <span> <b>{value[0]}</b></span></li>
                                            )                         
                                        }
                                       
                                    }
                                    
                                }
                                )}
                            </ul>
                        </ModalBody>
                    </div>
                </Modal>
            </>);
    }
    componentDidMount() {
        this.setState({
            eventKeyData: this.props.eventData.event_details_with_values,
            title:this.props.eventData.event_type
          })
    }

}
const mapStateToProps = (state) => {
    return {
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventModal);