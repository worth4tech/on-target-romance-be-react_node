export const eventData = [
    {
        "end": "2019-09-01",
        "name": "proof Reader",
        "Genre": "Historical",
        "blurb": "dfsdfsdf",
        "image": "/events/a4b00c6c-d17a-4ef8-adb2-d7b0e70d2c3e.png",
        "start": "2019-09-01",
        "title": "Title",
        "website": "sfsd",
        "category": "Adult",
        "coauthor": "co-author",
        "book-type": "Stand-alone",
        "undefined": "Real life issues,Inspirational,",
        "book-cover": "/events/a4b00c6c-d17a-4ef8-adb2-d7b0e70d2c3e.png",
        "Book-Number": "123",
        "event_title": "Book Release",
        "series-name": "test",
        "Pre-order links": [
            {
                "pre_order_country": "United States",
                "pre_order_site_link": "sdsdfsdf"
            }
        ],
        "Other Sites": [
            {
                "website_link": "sdfsdfsdfsdf",
                "website_type": "Barnes & Noble"
            }
        ]
    }
]
export const eventData1 = [
    {
        "Hosts": [
            {
                "host-link": "hl1",
                "hosted-by": "hb1",
                "host-contact": "hc1"
            },
            {
                "host-link_1": "hl2",
                "hosted-by_1": "hb2",
                "host-contact_1": "hc2"
            }
        ],
        "title": "titel",
        "coauther": "co-authorr",
        "start": new Date("2019-09-15T00:00:00.000Z"),
        "end": new Date("2019-09-15T00:00:00.000Z"),
        "Cover Reveal*": "Sun Sep 01 2019",
        "book-cover-link": "link to buy",
        "author-last-name": "aln",
        "author-first-name": "afn",
        "Connect to existing events": "SEARCH/SYNC"
    },

    {
        "Hosts": [
            {
                "host-link": "hosted link 1",
                "hosted-by": "hosted by 1",
                "host-contact": "hosted contact 1"
            },
            {
                "host-link_1": "hosted link 2",
                "hosted-by_1": "hosted by 2",
                "host-contact_1": "hosted contact 2"
            },
            {
                "host-link_2": "hosted link 3",
                "hosted-by_2": "hosted by 3",
                "host-contact_2": "hosted contact 3"
            }
        ],
        "title": "Cover reveal title",
        "coauther": "Co-auther name",
        "start": new Date("2019-09-12T00:00:00.000Z"),
        "end": new Date("2019-09-12T00:00:00.000Z"),
        "Cover Reveal": "Sun Sep 01 2019",
        "sign-up-link": "sign up link",
        "author-last-name": "Author last name",
        "author-first-name": "Auther first name",
        "Connect to existing events": "Search"
    },

    {
        "Hosts": [
            {
                "host-link": "hl1",
                "hosted-by": "hb1",
                "host-contact": "hc1"
            },
            {
                "host-link_1": "hl2",
                "hosted-by_1": "hb2",
                "host-contact_1": "hc2"
            }
        ],
        "title": "title",
        "coauther": "co author",
        "start": new Date("2019-09-10T00:00:00.000Z"),
        "end": new Date("2019-09-11T00:00:00.000Z"),
        "Book Cover": "/events/df8b8549-fb6c-4651-9a5a-9c53482fafa8.png",
        "ARC-Sign-up": "Open to All Readers and Bloggers",
        "sign-up-link": "signup link",
        "author-last-name": "aln",
        "author-first-name": "afn",
        "Connect to existing events": "Search / Sync"
    },

    {
        "Hosts": [
            {
                "host-link": "hl1",
                "hosted-by": "hb1",
                "host-contact": "hc1"
            },
            {
                "host-link_1": "hl2",
                "hosted-by_1": "hb2",
                "host-contact_1": "hc2"
            }
        ],
        "title": "title",
        "coauther": "co-author",
        "start": new Date("2019-09-14T00:00:00.000Z"),
        "end": new Date("2019-09-14T00:00:00.000Z"),
        "Cover Reveal*": "Sun Sep 01 2019",
        "book-cover-link": "link of event view",
        "author-last-name": "aln",
        "author-first-name": "afn",
        "Connect to existing events": "SEARCH/SYNC"
    },

    {
        "title": "Giveaway title",
        "coauthor": "Co-author name",
        "start": new Date("2019-09-16T00:00:00.000Z"),
        "end": new Date("2019-09-16T00:00:00.000Z"),
        "End date*": "Mon Sep 23 2019",
        "host-link": "Host link",
        "hosted-by": "Hosted by",
        "time-zone": "Timezon",
        "Book Cover": "/events/c441b70e-1a7c-4dce-bb07-adbfe289111e.png",
        "party-link": "Link to giveaway",
        "author-last-name": "Auther last name",
        "author-first-name": "Auther first name",
        "Give away start date*": "Sun Sep 01 2019",
        "Connect to existing events": "Search",
        "This party requires you to join group/page to participate.*": "Yes"
    },

    {
        "Type": "Stand-alone",
        "name": "formatter name",
        "Blurb": "blurb",
        "Genre": "Contemporary",
        "title": "title",
        "website": "goodreads link",
        "category": "Adult",
        "coauthor": "coauther",
        "start": new Date("2019-09-17T00:00:00.000Z"),
        "end": new Date("2019-09-17T00:00:00.000Z"),
        "Book Cover": "/events/cd7adee0-ec22-4c93-b656-861db90385ae.png",
        "Book-Number": "123",
        "Other Sites": [
            {
                "website_link": "Barnes link",
                "website_type": "Barnes & Noble"
            },
            {
                "website_link_1": "Google books",
                "website_type_1": "Google Books"
            }
        ],
        "series-name": "series name",
        "Release Date*": "Sun Sep 01 2019",
        "pre-order links": [
            {
                "pre_order_country": "United States",
                "pre_order_site_link": "us link"
            },
            {
                "pre_order_country_1": "Canada",
                "pre_order_site_link_1": "Canada link"
            }
        ],
        "book-trailer-link": "book trailer link",
        "Keyword(s) (check up to 3)": "BDSM,Real life issues,",
        "Sub-genre(s) (check up to 3)": "Dark,Paranormal/SciFi/Fantasy,"
    },

    {
        "title": "release party event title",
        "coauther": "cauthoer",
        "host-link": "hosted link",
        "hosted-by": "hosted by",
        "Book Cover": "/events/8777fed3-b468-4406-8a66-31423243ff1d.png",
        "party-link": "party link",
        "start": new Date("2019-09-18T00:00:00.000Z"),
        "end": new Date("2019-09-19T00:00:00.000Z"),
        "author-last-name": "aln",
        "author-first-name": "afn",
        "Release Party Date*": "Sun Sep 01 2019",
        "Connect to existing events": "Search",
        "List of Guest Authors (Optional)": "list of guest authoers",
        "Start Time (US Eastern Time Zone)*": {
            "start-time": "10:25"
        },
        "Release Party Event Name (should include book title)*": "release party name",
        "This party requires you to join group/page to participate": "Yes"
    }
]

