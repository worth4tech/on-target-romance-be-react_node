import moment from 'moment';
import React, { Component } from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import "react-big-calendar/lib/css/react-big-calendar.css";
import { connect } from 'react-redux';
import { setupEvent } from '../../Actions/event-setup';
import EventModal from './eventModal';

const localizer = momentLocalizer(moment)

class EventsCalendarView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            eventData: {},
            events: []
        }
        this.showEvent = this.showEvent.bind(this);
        this.hideEvent = this.hideEvent.bind(this);
    }

    showEvent(data) {
        this.setState({
            eventData: data,
            modal: true

        });
    }

    hideEvent() {
        this.setState({
            modal: false,
            eventData: {}
        });
    }

    eventStyleGetter(event) {
        var style = {
            background: "",
            borderRadius: '12px',
            opacity: 0.8,
            color: 'black',
            border: '0px',
            display: 'block',
            fontWeight: '600',
            width:'70px'
        };

        if (event.event_type === "event_type_1" || event.event_type === "Book Release") {
            style.background = "linear-gradient(to right,#65C1FF, #7193FF)"
        }
        if (event.event_type === "event_type_2" || event.event_type === "Giveaway") {
            style.background = "linear-gradient(to right,#9FFF81, #75ffc9)"
        }
        if (event.event_type === "event_type_3" || event.event_type === "Cover Reveal Promo") {
            style.background = "linear-gradient(to right,#FF964F, #ff8181)"
        }
        if (event.event_type === "event_type_4" || event.event_type === "Release Party") {
            style.background = "linear-gradient(to right,#FF79BC, #FF65FF)"
        }
        if (event.event_type === "event_type_5" || event.event_type === "Cover Reveal") {
            style.background = "linear-gradient(to right,#F3FfB5, #fFDB69)"
        }
        if (event.event_type === "event_type_6" || event.event_type === "Book Signing") {
            style.background = "linear-gradient(to right,#B469FF, #8E5BfF)"
        }
        if (event.event_type === "event_type_7" || event.event_type === "ARC Signup") {
            style.background = "linear-gradient(to right,#FF5B5B, #FF5B9D)"
        }
        return {
            style: style
        };
    }

    //===================== RENDER ==========================
    render() {
        let data = this.props.eventsData;
        if (data.length > 0) {
            data.forEach(element => {
                if (element["start"] === element["end"]) {
                    let end_date = new Date(element["end"]);

                    end_date.setHours(new Date(element["end"]).getHours() + 3)
                    element["start"] = new Date(element["start"])
                    element["end"] = new Date(end_date);

                } else {
                    element["start"] = new Date(element["start"])
                    element["end"] = new Date(element["end"]);
                }
            });

        }

        return (
            <>
                <div className={this.props.isTheameLight ? "calendar" : "calendar calendar-dark"}>
                    <Calendar
                        events={data}
                        step={30}
                        localizer={localizer}
                        defaultView='month'
                        views={['month', 'week', 'day']}
                        defaultDate={new Date()}
                        selectable
                        onSelectEvent={event => this.showEvent(event)}
                        eventPropGetter={(this.eventStyleGetter)}
                        messages={{agenda :"week"}}
                        
                    />
                </div>
                {this.state.modal ?
                    <EventModal isOpen={this.state.modal} eventData={this.state.eventData} toggle={this.hideEvent} />
                    : null}
            </>);

    }



}
const mapStateToProps = (state) => {
    return {
        loading: state.eventLoading
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setupEvent: () => dispatch(setupEvent()),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(EventsCalendarView);

