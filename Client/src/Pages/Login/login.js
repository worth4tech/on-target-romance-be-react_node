import React, { Component } from 'react';
import { connect } from 'react-redux';
import { authenticate,signuppage } from '../../Actions/index';
import { Route,Link } from 'react-router-dom';
import './login.css';

class Login extends Component {
    constructor(props){
        super(props)
        this.state={
            email:"",
            password:"",
            showpwd:false
        }
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
    }
  
    handleChangeEmail(e){
        this.setState({email:e.target.value});
    }
    handleChangePassword(e){
        this.setState({password:e.target.value});
    }
    handleLoginSubmit(e,history){
        e.preventDefault();
        this.props.fetchData("/api/v1/login",this.state.email,this.state.password,history);
    }

    componentWillReceiveProps(){
        // let myColor = { background: '#f8d7da', text: "#721c24"};
        // notify.show("Username or password is incorrect.", "custom", 5000, myColor);
      
      }
    render() {
        return (

            <div style={{ backgroundColor: "#1f1f1f" }}>
                <div className="container" >
                    <Logo />
                    <div className="row">
                        <div className="col-sm-12 col-md-10 col-lg-9 mx-auto">
                            <div className="my-5">
                            <Route render={({ history}) => (
                                <form className="form-signin" onSubmit={(e)=>{this.handleLoginSubmit(e,history)}}>
                                    <div className="row">
                                        <div className="col-12 col-sm-6">
                                            <div className="form-label-group form_gp_lft">
                                                <input type="email" id="inputEmail" className="form-control email" placeholder="Email"  onChange={this.handleChangeEmail} required autoFocus />
                                            </div>
                                        </div>
                                        <div className="col-12 col-sm-6">
                                            <div className="form-label-group form_gp_rgyt">                                            
                                                <input type={this.state.showpwd?"text":"password"}  id="inputPassword" className="form-control password" placeholder="Password"  onChange={this.handleChangePassword} required ></input> 
                                                <i className={"fa fa-eye"+ (this.state.showpwd?"-slash":"") +" pull-right show-pwd"} onClick={()=>{this.setState({showpwd:!this.state.showpwd})}}></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="custom-control custom-checkbox mb-4 mt-4">
                                        <input type="checkbox" id="check" style={{display:'none'}} />
                                        <label htmlFor="check" className="checkbox_label">
                                            <div className="mark"></div>
                                        </label>
                                        <p style={{ color: "#fff" }}>Remember me</p>

                                    </div>
                                    <div className="col-12 col-sm-3 mx-auto">
                                    
                                        <button className="btn btn-lg  btn-block  btn_login" type="submit">Login</button>
                                    
                                        
                                    </div>

                                    <div className="col-12 col-sm-10 mx-auto mb-4 mt-4">
                                        <ul className="social">
                                            <li style={{color:"#fff"}}>Login with Social Account</li>
                                            <li><Link to="#"><img src={require("../../Utils/Assets/facebook.png")} alt="facebook-logo"/></Link> </li>
                                            <li><Link to="#"><img src={require("../../Utils/Assets/insta.png")} alt="insta-logo"/></Link> </li>
                                            <li><Link to="#"><img src={require("../../Utils/Assets/twitter.png")} alt="twitter-logo"/></Link> </li>
                                        </ul>
                                    </div>
                                </form>
                                )}></Route>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>

        );
    }

    componentDidMount() {
        this.props.resetSignupPage();
    }
    

}

//{======= Logo Component ========}
const Logo = () => (
    <div className="row">
        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div className="logo my-5"> <a href="/log-in"><img src={require("../../Utils/Assets/logo.png")} alt="logo" /></a> </div>
        </div>
    </div>
);


//{======= Footter Component ========} 
const Footer = () => (
    <footer>
        <div className="container">
            <div className="row">
                <div className="col-12 mx-auto">
                    <div className="footer_contant"> <span style={{ color: "#fff" }}>Not yet a member?</span>
                    <Route render={({ history}) => (
                        <button className="btn btn_signup" type="submit" style={{ color: "#fff" }} onClick={()=>{history.push("/sign-up")}}>Sign up</button>
                        )}></Route>
                    </div>
                </div>
            </div>
        </div>
    </footer>
);

const mapStateToProps = (state) => {
    return {
      users: state.userlists,
      status: state.userloading
    }
  };
  
  const mapDispatchToProps = (dispatch) => {
    return {
      fetchData: (url,login,password,history) => dispatch(authenticate(url,login,password,history)),
      resetSignupPage: () => dispatch(signuppage(0))
    };
  };
  
  
  export default connect(mapStateToProps, mapDispatchToProps)(Login);