import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Logo from '../../Components/left-logo';
import HTMLParser from '../../Components/HTMLparser/html-parser'
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';
import { saveEvent, getEvent } from '../../Actions/event-setup';
import { notify } from 'react-notify-toast';
import { Route } from 'react-router-dom';
import "./event.css";
import eventButtons from '../../Utils/data/event-type-buttons.json'
const override = css`
    position:fixed;
    margin-top: 15%;
    left: 45%;
`;

class Event extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formJSON: [],
      currentFormObj: {},
      isSubmitted:false
    }

    this.htmlParser = new HTMLParser();
    this.arrprefillComponents = [
      { parenttag: "date", tag: "", value: new Date().toDateString() }
    ];
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onCheckboxChangeHandler = this.onCheckboxChangeHandler.bind(this);
    this.onCustomChangeHandler = this.onCustomChangeHandler.bind(this);
    this.onChangeFileHandler = this.onChangeFileHandler.bind(this);
    this.onSubmitFormHandler = this.onSubmitFormHandler.bind(this);
    this.addMoreLinkHandler = this.addMoreLinkHandler.bind(this);
    this.addMoreServiceHandler = this.addMoreServiceHandler.bind(this);
    this.removeServiceHandler = this.removeServiceHandler.bind(this);
    this.prefillComponentValueSubmitHandler = this.prefillComponentValueSubmitHandler.bind(this);
    this.onButtonClickHandler = this.onButtonClickHandler.bind(this);
    this.eventDetail = { name: "", event_type: "" };
    this.avtar = null;
  }


  //======================= On Change Handlers ================
  onButtonClickHandler(el) {
    if (el.eventTypeId != undefined) {
      this.eventDetail.name = el.value;
      this.eventDetail.event_type = el.eventTypeId;

      // this.props.getSchema("event_type_"+ el.eventTypeId);
      this.props.getSchema("/api/v1/events", { headers: { 'x-authorization': localStorage.getItem("token") } }, "event_type_" + el.eventTypeId);

    }
  }

  onChangeHandler(value, parentIndex, index) {
    console.log("value",value);
    
    let data = this.props.formJSON;
    let currentFormObjInState = this.state.currentFormObj;
    
    if (index != undefined && data[parentIndex].children != undefined) {  
      data[parentIndex].children[index].value = value;
      currentFormObjInState[data[parentIndex].children[index].name] = value;
    }

    if (data[parentIndex].dateValidationToIndex != undefined) {
      let i = parentIndex + parseInt(data[parentIndex].dateValidationToIndex);
      var itemDate = data[i].value === "" ? new Date() : new Date(data[i].value);
      if (itemDate.getTime() >= (new Date(value).getTime())) {
        notify.show("Start Date Can't be same/ahead of End Date.", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
        return;
      }
      else {
        data[parentIndex].value = value;
        currentFormObjInState[data[parentIndex].name] = value;
      }
    }
    if (data[parentIndex].dateValidationFromIndex != undefined) {
      let i = parentIndex + parseInt(data[parentIndex].dateValidationFromIndex);
      var itemDate = data[i].value === "" ? new Date() : new Date(data[i].value);
      if (itemDate.getTime() <= (new Date(data[parentIndex].value)).getTime()) {
        notify.show("Start Date Can't be ahead of End Date.", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
        return;
      }
      else {
        data[parentIndex].value = value;
        currentFormObjInState[data[parentIndex].name] = value;


      }
    }
    if (data[parentIndex].tag == "switch") {
      if (data[parentIndex].value == data[parentIndex].valueOn) {
        data[parentIndex].value = data[parentIndex].valueOff;
        currentFormObjInState[data[parentIndex].name] = data[parentIndex].valueOff;
      }
      else
        data[parentIndex].value = data[parentIndex].valueOn;
        currentFormObjInState[data[parentIndex].name] = data[parentIndex].valueOn;

    }
    else {
      data[parentIndex].value = value;
      currentFormObjInState[data[parentIndex].name] = value;


    }

    console.log("currentFormObjInState: ", JSON.stringify(currentFormObjInState));
    
    this.setState({ formJSON: data, currentFormObj: currentFormObjInState });
  }

  onCheckboxChangeHandler(value, parentIndex) {
    let data = this.props.formJSON;
    let currentFormObjInState = this.state.currentFormObj;

    if (data[parentIndex].value == undefined) {
      data[parentIndex].value = "";
    }
    if (data[parentIndex].value.includes(value)) {
      data[parentIndex].value = data[parentIndex].value.replace(value + ",", "");
      currentFormObjInState[data[parentIndex-1].title] = data[parentIndex].value.replace(value + ",", "");
    } else {
      if (data[parentIndex].validation == "ONLY 3" && data[parentIndex].value.replace(/(^,)|(,$)/g, "").split(',').length >= 3) {
        notify.show("Only 3 values allowed", "custom", 5000, { background: '#f8d7da', text: "#721c24" });


        return;
      }
      data[parentIndex].value = data[parentIndex].value.concat(value + ",");
      currentFormObjInState[data[parentIndex-1].title] = data[parentIndex].value.concat(value + ",");
    }


    // if(data[parentIndex].value.contains(value))
    // {
    //   data[parentIndex].value = data[parentIndex].value.replace(data[parentIndex].value).trim(",");
    // }
    // else{
    //   data[parentIndex].value += "," + value;
    // } 
    console.log("currentFormObjInState: for checkbox", JSON.stringify(currentFormObjInState));

    this.setState({ formJSON: data, currentFormObj: currentFormObjInState });
  }


  onCustomChangeHandler(value, index, parentIndex) {
    let currentFormObjInState = this.state.currentFormObj; 
    let data = this.props.formJSON;
    if (index != undefined && data[parentIndex].children != undefined) {
      if (data[parentIndex].dateValidationToIndex != undefined) {
        let i = parentIndex + parseInt(data[parentIndex].dateValidationToIndex);
        var itemDate = data[i].value === "" ? new Date() : new Date(data[i].value);
        console.log("date",itemDate);
        
        if (itemDate.getTime() > (new Date(data[parentIndex].value)).getTime()) {
          notify.show("End Date Can't be before of Start Date.", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
          return;
        }
        else {
          data[parentIndex].value = value;          
        }
      }
      if (data[parentIndex].dateValidationFromIndex != undefined) {
        let i = parentIndex + parseInt(data[parentIndex].dateValidationFromIndex);
        var itemDate = data[i].value === "" ? new Date() : new Date(data[i].value);
        if (itemDate.getTime() < (new Date(data[parentIndex].value)).getTime()) {
          notify.show("Start Date Can't be ahead of End Date.", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
          return;
        }
        else {
          data[parentIndex].value = value; 
        }
      }
      if (data[parentIndex].children[index].tag == "switch") {
        if (data[parentIndex].children[index].value == data[parentIndex].children[index].valueOn) {
          data[parentIndex].children[index].value = data[parentIndex].children[index].valueOff;
        }
        else
          data[parentIndex].children[index].value = data[parentIndex].children[index].valueOn;
      }
      else{
        data[parentIndex].children[index].value = value;
        currentFormObjInState[data[parentIndex].children[index].name] = value;
      }
        
    }
    else {
      data[parentIndex].value = value;
      // currentFormObjInState[data[parentIndex].name] = value;
    }
    this.setState({ formJSON: data , currentFormObj:currentFormObjInState });
  }

  onChangeFileHandler(file, acceptedSize) {
    if (parseInt(acceptedSize) * 1000 <= parseInt(file.size)) {
      notify.show("File size is bigger than max size. Event will be saved without Image.", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
      return;
    }
    this.avtar = file;
  }

  onSubmitFormHandler(e, history) {
    this.setState({isSubmitted:true})
    e.preventDefault();
    if (this.props.formJSON.length == 0) {
      history.push("/dashboard");
      return;
    }
    //this.prefillComponentValueSubmitHandler();

    let data = this.state.currentFormObj;
    if (!data["start_date"]) {
      notify.show("Please fill all Manadatory fields", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
          return;
    }
    if(data.end_date===""){
      notify.show("Please fill all Manadatory fields", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
          return;
    }

    if (!data.end_date) {
      data["end_date"] = data["start_date"]
    }
    if (!data["author-first-name"]) {
      data["author_first_name"] = ""
    } else {
      data["author_first_name"] = data["author-first-name"];
      delete data["author-first-name"];
    }
    if (!data["author-last-name"]) {
      data["author_last_name"] = ""
    } else {
      data["author_last_name"] = data["author-last-name"];
      delete data["author-last-name"];
    }
    if (!data["hosted-by"]) {
      data["hosted_by"] = ""
    } else {
      data["hosted_by"] = data["hosted-by"];
      delete data["hosted-by"];
    }
    if (!data["start-time"]) {
      data["time"] = ""
    } else {
      data["time"] = data["start-time"]
      delete data["start-time"];
    }

    if (!data.title) {
      data['title'] = this.eventDetail.name;
    }
    
    let formData = new FormData();
    formData.append("event_details", JSON.stringify(this.props.formJSON));
    
    if (this.avtar) {
      formData.append("avtar", this.avtar);
    }

    formData.append("event_type", "event_type_" + this.eventDetail.event_type);
    formData.append("event_details_with_values", JSON.stringify(data));

    //this.getJSONFormData(); ytrmz
    const headers = { 'content-type': 'multipart/form-data', 'x-authorization': localStorage.getItem("token") };
    this.props.saveUserEvent("/api/v1/event/", headers, formData, history);
  }

  prefillComponentValueSubmitHandler() {

    let data = this.props.formJSON;

    this.arrprefillComponents.forEach(element => {
      data.forEach(jsonelement => {
        if (jsonelement.tag === element.parenttag) {
          if (element.tag != "") {
            jsonelement.children.forEach(childelement => {
              if (childelement.tag == element.tag) {
                if (childelement.value === "" || childelement.value == null) {
                  childelement.value = element.value;
                }
              }
            });
          } else {
            if (jsonelement.value === "" || jsonelement.value == null) {
              jsonelement.value = element.value;
            }
          }
        }
      });
    });
    this.setState({ formJSON: data });
  }

  addMoreLinkHandler(parentIndex, index) {
    let data = this.props.formJSON;
    data[parentIndex].children.push(data[parentIndex].children[index]);
    let el = {
      "tag": "input",
      "type": "text",
      "className": "form-control in_form_control",
      "placeholder": "link",
      "value": "",
      "validation": false,
      "name": ""
    };
    data[parentIndex].children[index] = el;
    data[parentIndex].children[index].name = data[parentIndex].linkname + (parseInt(data[parentIndex].links) + 1);
    data[parentIndex].links = parseInt(data[parentIndex].links) + 1;

    this.setState({ formJSON: data });
  }
  
  addMoreServiceHandler(parentIndex, index) {
    let data = this.props.formJSON;
    let addMoreBtn = data[parentIndex].children[data[parentIndex].children.length - 1];
    let minusBtn = { "tag": "minus", "type": "service", "placeholder": "Remove" };

    let arrChildData = data[parentIndex].children;

    arrChildData.splice(index, 1);
    if (data[parentIndex].childrenobj == undefined) {
      // debugger;
      data[parentIndex].childrenobj = [];
      data[parentIndex].childrenobj = data[parentIndex].childrenobj.concat(arrChildData);

    }
    let arrChildData_deepcopy = JSON.parse(JSON.stringify(data[parentIndex].childrenobj));
    arrChildData_deepcopy.forEach(function (el, i) {
      // debugger;
      if (data[parentIndex].children.count != undefined && i > data[parentIndex].children.count) { return; }
      el.name += "_" + data[parentIndex].service;
      el.value = "";
      data[parentIndex].children = data[parentIndex].children.concat([el]);
      
    });

    // Increase service count
    data[parentIndex].service = parseInt(data[parentIndex].service) + 1;

    // data[parentIndex].children = data[parentIndex].children.concat(data[parentIndex].childrenobj);
    minusBtn.index = data[parentIndex].children.length;
    data[parentIndex].children.push(minusBtn);
    data[parentIndex].children.push(addMoreBtn);


    this.setState({ formJSON: data });
  }

  removeServiceHandler(parentIndex, index) {
    let data = this.props.formJSON;
    data[parentIndex].children.splice(index - data[parentIndex].childrenobj.length, data[parentIndex].childrenobj.length + 1);
    // Decrease service count
    data[parentIndex].service = parseInt(data[parentIndex].service) - 1;

    this.setState({ formJSON: data });
  }

  //======================= On Change Handlers ENDS ================


  //======================= Common functions ====================


  //===================== RENDER ==========================
  render() {
    let isSubmitted= this.state.isSubmitted;
    console.log(this.props.formJSON);
   
    return (
      <>
        <div style={{ backgroundColor: "#1f1f1f" }}>
          <div className="container-fluid">
            <div className="row">
              <Logo />
              <div className="main_right author_profile" >
                <div className="setup_p_hed">
                  <h4>Set-up Event: {this.eventDetail.name} </h4>
                  <a href="/">
                    <button type="button" className="btn" onClick={() => { localStorage.removeItem("token"); return (<Redirect to="/" push />) }} style={{ float: "right" }}>LogOut</button></a>
                </div>
                <Route render={({ history }) => (

                  <form className="form_custom" onSubmit={(e) => (this.onSubmitFormHandler(e, history))}>
                    {
                      this.props.formJSON.length === 0 ?
                        eventButtons.map((el, index) =>
                          //this.addPrefillValue(el, index)
                          this.htmlParser.getFormElements(el, index, this.onChangeHandler, this.onCustomChangeHandler, this.onChangeFileHandler, this.onCheckboxChangeHandler, this.addMoreServiceHandler, this.removeServiceHandler, this.addMoreLinkHandler, this.onButtonClickHandler,0,isSubmitted)
                        )
                        : this.props.formJSON.map((el, index) =>
                          this.htmlParser.getFormElements(el, index, this.onChangeHandler, this.onCustomChangeHandler, this.onChangeFileHandler, this.onCheckboxChangeHandler, this.addMoreServiceHandler, this.removeServiceHandler, this.addMoreLinkHandler, this.onButtonClickHandler,0,isSubmitted)
                        )


                    }
                    {this.props.formJSON.length != 0 ? (

                      <div className="full_title">
                        <label>*All events can be edited by account owner. Go to event page, click on edit box in upper right corner</label>
                        <div className="form-group">
                          <button type="submit" className="btn btn_save_bt">Save</button>
                        </div>
                      </div>
                    ) : (<div className="full_title" style={{ marginTop: "14%" }}>
                      <div className="form-group">
                        <button type="submit" className="btn btn_save_bt">Skip</button>

                      </div>
                    </div>
                      )}
                  </form>
                )}></Route>

              </div>
            </div>
          </div>
        </div>
      </>
    )




  }

  componentDidMount() {
    this.props.getSchema("/api/v1/events", { headers: { 'x-authorization': localStorage.getItem("token") } }, "event_type_0");
  }

  componentWillReceiveProps() {
    this.setState({ formJSON: this.props.formJSON });
  }

}

const mapStateToProps = (state) => {
  return {
    loading: state.eventLoading,
    formJSON: state.eventData,
    eventDetail: state.eventDetail

  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveUserEvent: (url, headers, data, history) => dispatch(saveEvent(url, headers, data, history)),
    getSchema: (url, headers, schema_name) => dispatch(getEvent(url, headers, schema_name))
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Event);


