import 'ag-grid-community';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import 'ag-grid-community/dist/styles/ag-theme-dark.css';
import { AgGridReact } from 'ag-grid-react';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { HashLoader } from 'react-spinners';
import { getUserList, setupEvent } from '../../Actions/profile-setup';
import Logo from '../../Components/left-logo';
import UserProfileModal from './userProfileModal';
import './usersList.css';

class UsersListView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usersList: [],
      profileData: {},
      modal: false,
      myevents: false,
      columnDefs: [
        {
          headerName: "ID", field: "id", hide: true
        },
        {
          headerName: "DISPLAY NAME", field: "display_name"
        },
        {
          headerName: "EMAIL", field: "email"
        },
        {
          headerName: "EMAIL VERIFIED", field: "is_email_verified", sortable: true, filter: true, resizable: true,
          cellRenderer: function (is_email_verified) {
            if (is_email_verified.value === 1) {
              return "YES"
            }else{
              return "NO"
            }
          },
        },
        {
          headerName: "COUNTRY ID", field: "country_id"
        },
        {
          headerName: "MEMBER EXPIRY DATE", field: "member_expiry_date"
        },


      ],
      defaultColDef: {
        editable: true,
        sortable: true,
        resizable: true,
        filter: true
      },
      paginationPageSize: 10,
      paginationNumberFormatter: function (params) {
        return "" + params.value.toLocaleString() + "";
      },
      displayedRows: 0,
      showMenu: false,
    }
    this.props.setupEvent();
    this.hideEvent = this.hideEvent.bind(this);
    this.hideDropDown = this.hideDropDown.bind(this);
    this.showDropDown = this.showDropDown.bind(this);
  }

  showDropDown() {
    this.setState({
      showMenu: true
    });
  }

  hideDropDown() {
    this.setState({
      showMenu: false
    });
  }

  handleMyEvents(event) {
    this.setState({
      myevents: event.target.checked
    });
  }

  showEvent(e) {
    this.setState({
      modal: true,
      profileData: e.data
    });
  }

  hideEvent() {
    this.setState({
      modal: false,
      profileData: {}
    });
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.columnApi = params.columnApi;
    this.gridApi.sizeColumnsToFit();
    this.gridApi.paginationGoToPage(this.props.current);
    this.gridApi.setDomLayout("autoHeight");
  }

  refreshGrid() {
    this.props.getCases();
  }

  onPageSizeChanged(newPageSize) {
    var value = document.getElementById("page-size").value;
    this.gridApi.paginationSetPageSize(Number(value));
  }

  onFilterChanged(e) {
    var value = document.getElementById("search").value;
    this.gridApi.setQuickFilter(value);
    this.setState({ displayedRows: this.gridApi.getDisplayedRowCount() });
  }
  //===================== RENDER ==========================
  render() {
    // if (this.props.loading) {
    //   return (<div className='sweet-loading'>
    //     <HashLoader
    //       sizeUnit={"px"}
    //       size={100}
    //       color={'#1F1F1F'}
    //       loading={true}
    //     />
    //   </div>)
    // }
    return (
      <>
        <div style={{ backgroundColor: "#1f1f1f" }}>
          <div className="container-fluid">
            <div className="row">
              <Logo />
              <div className="main_right author_profile" style={{ minHeight: 1000 + 'px' }}>
                <div className={this.state.myevents ? "navigation bg-light" : "navigation bg-dark"}>
                  <nav className={this.state.myevents ? "navbar navbar-expand-lg navbar-light bg-light" : "navbar navbar-expand-lg navbar-light bg-dark"}>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                          <a className="nav-link" href="#">Home</a>
                        </li>
                        <li className="nav-item">
                          <Link className="nav-link" to={'/dashboard'}>Calendar</Link>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="#">Event filter</a>
                        </li>
                        <li className="nav-item active">
                          <Link className="nav-link" to={'/user-list'}>LISTS</Link>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="#">VENDORS</a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="#">daily task</a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link" href="#">PROFILE</a>
                        </li>
                      </ul>
                    </div>
                  </nav>
                  <div className="noti_setting"><a className="notification" href="#">4</a>
                    <a style={{ marginRight: "40px" }} className="dropdown">
                      <img onClick={() => this.showDropDown()} src={require("../../Utils/Assets/setting_icon.png")} alt="setting" />
                      <div className={this.state.showMenu ? "dropdown-content show" : "dropdown-content"}>
                        <a href="/">Profile</a>
                        <Link to={'/user-list'}>Users List</Link>
                        <a onClick={() => { localStorage.removeItem("token"); return (<Redirect to="/" push />) }}>Log Out</a>
                      </div>
                    </a>
                  </div>
                </div>

                <div className="filter_row">
                  <div className="row">
                    <div className="col-sm-12">
                    </div>
                  </div>
                  <div className="filter_options">
                    <div className="row">
                      <div className="col-sm-12">
                        <div className="toggle_switch">
                          <span className="yes_no">Dark</span>
                          <label className="switch">
                            <input type="checkbox" name="myevents" onChange={this.handleMyEvents.bind(this)} />
                            <span className="slider round"></span>
                          </label>
                          <span className="yes_no">Light</span>
                        </div>
                        <div className="sport">
                          <a href="#">
                            <span>Sort</span>
                            <img src={require("../../Utils/Assets/sort_icon.png")} alt="sort-icon" />
                          </a>
                        </div>
                        <div className="new_filter">
                          <a href="#">
                            <img src={require("../../Utils/Assets/cup_icon.png")} alt="cup-icon" />
                          </a>
                        </div>
                        <div className="search_box">
                          <form>
                            <input type="text" placeholder="Search" name="search" />
                            <button type="submit"><img src={require("../../Utils/Assets/search_icon.png")} alt="search-icon" /></button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className={this.state.myevents ? "ag-theme-balham" : "ag-theme-dark home-ag"} style={{}}>
                  <AgGridReact
                    onGridReady={this.onGridReady}
                    columnDefs={this.state.columnDefs}
                    rowData={this.props.userList}
                    pagination={true}
                    reactNext={true}
                    animateRows
                    onCellClicked={this.showEvent.bind(this)}
                    paginationPageSize={this.state.paginationPageSize}
                    paginationNumberFormatter={this.state.paginationNumberFormatter}
                  />
                  <div className="test-header" style={{ float: "right" }}>
                    Page Size:
                         <select onChange={this.onPageSizeChanged.bind(this)} id="page-size" defaultValue="10">
                      <option value="10" >10</option>
                      <option value="100">100</option>
                      <option value="500">500</option>
                      <option value="1000">1000</option>
                      <option value="1000">1000</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div >
        {
          this.state.modal ?
            <UserProfileModal isOpen={this.state.modal} profileData={this.state.profileData} toggle={this.hideEvent} />
            : null
        }
      </>);

  }

  componentDidMount() {
    this.props.getUserList("/api/v1/users", { headers: { 'x-authorization': localStorage.getItem("token") } });
  }

}

const mapStateToProps = (state) => {
  return {
    loading: state.profileLoading,
    userList: state.userListData,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    setupEvent: () => dispatch(setupEvent()),
    getUserList: (url, headers) => dispatch(getUserList(url, headers))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersListView);