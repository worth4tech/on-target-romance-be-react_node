import React, { Component } from 'react';
import { connect } from 'react-redux';
import { HashLoader } from 'react-spinners';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

class UserProfileModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profileKeyData: []
        }
    }

    //===================== RENDER ==========================
    render() {
        const self = this;
        return (
            <>
                <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} id="myModal">
                    <div className="model_cover">
                        <ModalHeader toggle={this.props.toggle}>
                            Profile Info
                        </ModalHeader>
                        <ModalBody>
                            <ul>
                                {this.state.profileKeyData.map((key) => {
                                    if (key === "id" || key === "password" || key === "created_date" || key === "updated_date" || key === "user_details") {
                                        return
                                    }
                                    else {
                                        if (self.props.profileData && self.props.profileData[key] === null) {
                                            return
                                        } else if (Object.prototype.toString.call(self.props.profileData && self.props.profileData[key]) === "[object Object]") {
                                            alert()
                                            return
                                        } else if (Object.prototype.toString.call(self.props.profileData && self.props.profileData[key]) === "[object Array]") {
                                            return
                                        } else {
                                            return <li><span>{key}</span> : <span><b>{self.props.profileData && self.props.profileData[key]}</b></span></li>
                                        }
                                    }
                                }
                                )}
                            </ul>
                        </ModalBody>
                    </div>
                </Modal>
            </>);
    }

    componentDidMount() {
        this.setState({
            profileKeyData: Object.getOwnPropertyNames(this.props.profileData)
        })
    }
}
const mapStateToProps = (state) => {
    return {
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(UserProfileModal);
