import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Logo from '../../Components/left-logo';
import TextBox from '../../Components/text-box';
import SelectBox from '../../Components/select-box';
import FileBox from '../../Components/file-box';
import DateBox from '../../Components/date-box';
import { HashLoader } from 'react-spinners';
import { css } from '@emotion/core';
import { saveProfile, getProfile } from '../../Actions/profile-setup';
import { notify } from 'react-notify-toast';
import './home.css'
import { stat } from 'fs';
import { Route} from 'react-router-dom';

const override = css`
    position:fixed;
    margin-top: 15%;
    left: 45%;
`;

var position = 0; 
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formJSON: [],
      profile: null
    }
    this.arrprefillComponentValues = [];
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onCheckboxChangeHandler = this.onCheckboxChangeHandler.bind(this);
    this.onCustomChangeHandler = this.onCustomChangeHandler.bind(this);
    this.onChangeFileHandler = this.onChangeFileHandler.bind(this);
    this.onSubmitFormHandler = this.onSubmitFormHandler.bind(this);
    this.getCustomElementObject = this.getCustomElementObject.bind(this);
    this.addMoreLinkHandler = this.addMoreLinkHandler.bind(this);
    this.addMoreServiceHandler = this.addMoreServiceHandler.bind(this);
    this.prefillComponentValueSubmitHandler = this.prefillComponentValueSubmitHandler.bind(this);
  }


  //======================= On Change Handlers ================
  onChangeHandler(value, index) { 
    let data = this.state.formJSON;
    data[index].value = value;
    this.setState({ formJSON: data });
  }
  onCheckboxChangeHandler(value, parentIndex) {
    let data = this.state.formJSON;
    if(data[parentIndex].value == undefined){
      data[parentIndex].value = "";
    }
    if(data[parentIndex].value.includes(value)) {
      data[parentIndex].value = data[parentIndex].value.replace(value, "");
    }
    else{

      data[parentIndex].value = data[parentIndex].value.concat(value);
    }

    // if(data[parentIndex].value.contains(value))
    // {
    //   data[parentIndex].value = data[parentIndex].value.replace(data[parentIndex].value).trim(",");
    // }
    // else{
    //   data[parentIndex].value += "," + value;
    // } 
    this.setState({ formJSON: data });
  }

  onCustomChangeHandler(value, index, parentIndex) {
    let data = this.state.formJSON;
    data[parentIndex].children[index].value = value;
    this.setState({ formJSON: data });
  }

  onChangeFileHandler(file, acceptedSize) {
    if(parseInt(acceptedSize) * 1000 <= parseInt(file.size)){
      notify.show("File size is bigger than max size. Profile will be saved without Image.", "custom", 5000, { background: '#f8d7da', text: "#721c24" });
      return;
    }
    this.setState({ profile: file });
  }

  onSubmitFormHandler(e, history) {

    e.preventDefault();
  
    this.prefillComponentValueSubmitHandler();
    let formData = new FormData();
    formData.append("details", JSON.stringify(this.state.formJSON));
    formData.append("profile", this.state.profile);
    //this.getJSONFormData(); ytrmz
    const headers = { 'content-type': 'multipart/form-data', 'x-authorization': localStorage.getItem("token") };
    this.props.saveUserProfile("/api/v1/profile/setup", headers, formData, history);
  }

  prefillComponentValueSubmitHandler() {
    (this.arrprefillComponentValues).forEach(element => {
      let data = this.state.formJSON;
      if (data[element.split(',')[2]].children[element.split(',')[1]].value === "") {
        data[element.split(',')[2]].children[element.split(',')[1]].value = element.split(',')[0];
        this.setState({ formJSON: data });
      }
    });
  }

  addMoreLinkHandler(parentIndex, index) {
    let data = this.state.formJSON;
    data[parentIndex].children.push(data[parentIndex].children[index]);
    let el = {
      "tag": "input",
      "type": "text",
      "className": "form-control in_form_control",
      "placeholder": "link",
      "value": "",
      "validation": false,
      "name": ""
    };
    data[parentIndex].children[index] = el;
    data[parentIndex].children[index].name = data[parentIndex].linkname + (parseInt(data[parentIndex].links) + 1);
    data[parentIndex].links = parseInt(data[parentIndex].links) + 1;

    this.setState({ formJSON: data });
  }

  addMoreServiceHandler(parentIndex, index) {
    let data = this.props.formJSON;
    let addMoreBtn = data[parentIndex].children[data[parentIndex].children.length - 1];
    let minusBtn = {"tag": "minus", "type": "service", "placeholder": "Remove"};
    
    let arrChildData = data[parentIndex].children;
    
    arrChildData.splice(index,1);
    if(data[parentIndex].childrenobj == undefined){
      // debugger;
      data[parentIndex].childrenobj = [];
      data[parentIndex].childrenobj = data[parentIndex].childrenobj.concat(arrChildData);
       
    }
    let arrChildData_deepcopy = JSON.parse(JSON.stringify(data[parentIndex].childrenobj));
    arrChildData_deepcopy.forEach(function(el, i){
     // debugger;
      if(data[parentIndex].children.count != undefined && i> data[parentIndex].children.count){ return;}
      el.name += "_" + data[parentIndex].children.length;  
      el.value = "";
      data[parentIndex].children = data[parentIndex].children.concat([el]);
      
    });

   // data[parentIndex].children = data[parentIndex].children.concat(data[parentIndex].childrenobj);
    minusBtn.index = data[parentIndex].children.length;
    data[parentIndex].children.push(minusBtn);
    data[parentIndex].children.push(addMoreBtn);
    
   
    this.setState({ formJSON: data });
  }
  
  //======================= On Change Handlers ENDS ================


  //======================= Common functions ====================

  getJSONFormData() {
    const formData = new FormData();
    this.state.formJSON.map(el => {
      if (el.name) {
        if (el.tag === "file")
          formData.append(el.name, this.state.profile);
        else if (el.tag === "custom") {

          let data = this.getCustomElementObject(el.children);

          formData.append(el.name, data);
        }
        else
          formData.append(el.name, el.value);
      }
    });
    return formData;
  }

  getCustomElementObject(children) {
    let data = {};
    children.map(el => {
      if (el.name) {
        data[el.name] = el.value;
      }
    });
    return JSON.stringify(data);
  }

  //======================= Common functions ENDS ====================

  //======================= Custom DOM Providers ================

  getCheckBoxes(attributes, parentIndex, IsUnderCustom){
    return attributes.buttons.map((el,i)=>{
      var checkboxid = "exampleCheck" + i;
      return(
        <div className="custom-control custom-checkbox white col-md-4 " style={{ float :"left"}}>
        <input type="checkbox" checked={attributes.value !== undefined && attributes.value.includes(el.name)}
            onChange={() => this.onCheckboxChangeHandler(el.name, parentIndex)} id={checkboxid} style={{display : "none"}}/>
          <label htmlFor={checkboxid} className="checkbox_label_black">
        <div className="mark"></div>
        </label><p>{el.name}</p>
        </div>
       
        // <div className="lft" style={{ marginTop: "10px" }}>
        //  <input type="checkbox" checked={this.state.formJSON[parentIndex].value !== undefined && this.state.formJSON[parentIndex].value.includes(el.name)}
        //     onChange={() => this.onCheckboxChangeHandler(el.name, parentIndex)} id={checkboxid} /> 
        
        //         <span>{el.name}</span>  
        // </div>
      )
    })
  }

  getRadioButtons(attributes, index, parentIndex, IsUnderCustom) {
    return attributes.buttons.map((el, i) => {
      return (
        <label key={el.name} className="raemail">
          <input
            type="radio" name={attributes.name}
            checked={this.state.formJSON[index].value === el.name}
            onChange={() => this.onChangeHandler(el.name, index)} /> {el.name}
        </label>
        //  <div className="rdio rdio-primary radio-inline radioninline_3">
        //  <input name={attributes.name} type="radio" className="author-radio"  checked={attributes.value === el.name}
        //      onChange={() => this.onChangeHandler(el.name, index)} />
        //  <label key={el.name}  htmlFor={"radio_"+parentIndex +"_" + i}>{el.name}</label>
        //  </div>
      )
    });

  }

  getTextAreaElement(attributes, index, parentIndex, IsUnderCustom) {
    return (
      <textarea type={attributes.type} value={attributes.value ? attributes.value : ""} className={attributes.className} placeholder={attributes.placeholder} onChange={(e) => IsUnderCustom ? this.onCustomChangeHandler(e.target.value, index, parentIndex) : this.onChangeHandler(e.target.value, index)} required={attributes.validation} ></textarea>
    );
  }

  getTitleElement(attributes) {
    return (<label>{attributes.title}:</label>)
  }

  //======================= Custom DOM Providers ENDS ================


  //======================= Custom Component Providers ================

  getCustomComponent(attributes, parentIndex) {
    let pos = 0;
    return attributes.children.map((el, index) => {
      switch (el.tag) {
        case "input":
          return <div className={pos % 2 === 0 ? "rght" : "lft"}  key={index + "_" + parentIndex}>
            <TextBox
              attributes={el}
              IsUnderCustom={true}
              onCustomHandler={this.onCustomChangeHandler}
              onChangeTextHandler={this.onChangeHandler}
              index={index}
              parentIndex={parentIndex}
            />
          </div>
        case "select":
          return (
            <div className={pos % 2 === 0 ? "rght" : "lft"} >
            <div className="dropdown" key={index + "_" + parentIndex}>
              <SelectBox
                attributes={el}
                IsUnderCustom={true}
                onCustomHandler={this.onCustomChangeHandler}
                onChangeTextHandler={this.onChangeHandler}
                index={index}
                parentIndex={parentIndex}
              />
            </div>
            </div>
          )
        case "title":
          return (
            <div className="full_title" key={index}>
              {this.getTitleElement(el)}
            </div>
          )

          case "date":
          return (
            
            <div className="lft" key={index}>
              <div className="form-group">
                <DateBox
                  attributes={el}
                  index={index}
                  IsUnderCustom={true}
                  parentIndex={parentIndex}
                  onCustomHandler={this.onCustomChangeHandler}
                  onChangeDateHandler={this.onChangeHandler} />
                   
                   </div>
                   <div style={{ display : "none"}}>
                   {
                    this.arrprefillComponentValues = this.arrprefillComponentValues.concat([(el.value === "" ? new Date().toDateString() : el.value) + ',' + index + ',' + parentIndex])
                  }
                  </div>
            </div>
            
          )

        case "service":
          return (

            <div className="rght" key={index}>
              <div className="form-group">
                <TextBox
                  attributes={el}
                  IsUnderCustom={true}
                  onCustomHandler={this.onCustomChangeHandler}
                  onChangeTextHandler={this.onChangeHandler}
                  index={index}
                  parentIndex={parentIndex}
                />
              </div>
            </div>
          )
        
        case "add":
          return (
            <div className="ft_text" key={"plus" + index}><span >{el.placeholder} <img src={require("../../Utils/Assets/plus.png")} onClick={() => {el.type==="service"?this.addMoreServiceHandler(parentIndex, index) :this.addMoreLinkHandler(parentIndex, index) }} className="addMore" /> </span></div>
          )

      }

    })
  }

  //======================= Custom Component Providers ENDS ================

  getFormElements() {
      // 1:left  2:right
    //console.log("this.state.formJSON", this.state.formJSON);
    return this.state.formJSON.map((el, index) => {
      el.tag === "title" || el.tag === "custom" ? position = 0 : position++;
      switch (el.tag) {
        case "blank":
          return(<div className={position % 2 === 0 ? "rght" : "lft"} key={index}></div>)
  
        case "input":
          return (
            <div className={position % 2 === 0 ? "rght" : "lft"} key={index}>
              <div className="form-group">
                <TextBox
                  attributes={el}
                  IsUnderCustom={false}
                  onCustomHandler={this.onCustomChangeHandler}
                  onChangeTextHandler={this.onChangeHandler}
                  index={index}
                  parentIndex={0}
                />
              </div>
            </div>
          )
        case "select":
          return (
            <div className={position % 2 === 0 ? "rght" : "lft"} key={index}>
              <div className="form-group">
                <SelectBox
                  attributes={el}
                  IsUnderCustom={true}
                  onCustomHandler={this.onCustomChangeHandler}
                  onChangeTextHandler={this.onChangeHandler}
                  index={index}
                  parentIndex={0}
                />

              </div>
            </div>
          )
        case "title":
          return (
            <div className="full_title" key={index}>
              {this.getTitleElement(el)}
            </div>
          )
        case "textarea":
          return (
            <div className="full_title" key={index}>
              <div className="form-group">
                {this.getTextAreaElement(el, index, 0, false)}
              </div>
            </div>
          )
        case "file":
          return (
            <div className={position % 2 === 0 ? "rght" : "lft"} key={index}>
              <div className="form-group">
                <FileBox attributes={el} index={index} parentIndex={0} onFileHandler={this.onChangeFileHandler} />
              </div>
            </div>
          )
        case "radio":
          return (
            <div className={position % 2 === 0 ? "rght" : "lft"} key={index}>
              <div className="form-group">
                <span style={{ color: "#1f1f1f", fontWeight: "600" }}>{el.placeholder}</span>
                {this.getRadioButtons(el, index, 0, false)}
              </div>
            </div>
          );

          case "checkbox":
          return (
            <div key={index}>
              
              {/* <div className="checkbox">
              <label><input type="checkbox" value="true" />Option 1</label>
              </div> */}
                 {this.getCheckBoxes(el, index, 0, false)} 
              
            </div>
          );
        case "custom":
          return (
            <div className="full_title" key={index}>
              <div className="form-group fam_gp">
                <div className="in_form">
                  <div className="in_title"> <label>{el.title}:</label> </div>
                  {this.getCustomComponent(el, index)}
                </div>
              </div>
            </div>
          )
        case "customdate":
          return (
            <div className="full_title" key={index}>
              <div className="form-group fam_gp">
                <div className="in_form">
                  <div className="in_title"> <span>{el.placeholder}:</span> </div>
                  {this.getCustomComponent(el, index)}
                </div>
              </div>
            </div>
          )

      }

    })
  }



  //===================== RENDER ==========================
  render() {
    return (
      this.state.formJSON?
      <>
        <div style={{ backgroundColor: "#1f1f1f" }}>
          <div className="container-fluid">
            <div className="row">
              <Logo />
              <div className="main_right author_profile" >
                <div className="setup_p_hed">
                  <h4>Set-up Profile: {this.props.userType} </h4>
                  <a href="/">
                    <button type="button" className="btn" onClick={() => { localStorage.removeItem("token"); return (<Redirect to="/" push />) }} style={{ float: "right" }}>LogOut</button></a>
                </div>
                <Route render={({ history}) => (
                                 
                <form className="form_custom" onSubmit={(e)=>(this.onSubmitFormHandler(e ,history))}>
                  {this.state.formJSON ? this.getFormElements() : ""}
                 
                  <div className="full_title">
                      <label> To edit, go to your profile page, and click on edit box in the upper right corner.</label>
                      <div className="form-group">
                          <button type="submit" className="btn btn_save_bt">Save</button>
                        </div>
                  </div>
                  
                </form>
                )}></Route>

              </div>
            </div>
          </div>
        </div>
      </>:"");

  }

  componentWillMount() {
   position = 0;
   if(localStorage.getItem("token")){
    this.props.getUserProfile("/api/v1/user/profile", { headers: { 'x-authorization': localStorage.getItem("token") } }); 
   }else{
     this.props.history.push("/log-in")
   } 
  }

  componentWillReceiveProps() {
    this.setState({ formJSON: this.props.formJSON });
  }
 
}

const mapStateToProps = (state) => {
  return {
    loading: state.profileLoading,
    formJSON: state.profileData,
    userType: state.userType
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    saveUserProfile: (url, headers, data, history) => dispatch(saveProfile(url, headers, data, history)),
    getUserProfile: (url, headers) => dispatch(getProfile(url, headers))
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Home);


