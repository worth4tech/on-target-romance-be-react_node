import React,{Component} from 'react';
import ReactHtmlParser from 'react-html-parser'
import { css } from '@emotion/core';
import $ from 'jquery';
export default class Checkout extends Component{

    constructor(props){
        super(props);
        this.confirmPayment = this.confirmPayment.bind(this);
    }

    confirmPayment(){
        this.props.checkout();
    }

    render() {
      return (<>
        <div className="author_main_s">
          <div className="modal-body_s">
            <h1>{this.props.member} Membership</h1>
            <ul className="model_text">
              {ReactHtmlParser(this.props.details)}
            </ul>
          </div>
        </div>

      <div className="frame-container">
        <div className="frame-header">
          <span className="title">Launch Specials</span>
        </div>
        <div className="frame-content">
          <div className="m-t-10">Limited one-time offer</div>
          <div className="row m-t-10">
            <input type="radio" name="launch-offer" id="18-months" value="18"></input>
            <label className="radio-label" for="18-months">$260 USD for 18 months. A $360 USD value. <span className="red-colored">Save $120 USD.</span></label>
          </div>
          <div className="row">
            <input type="radio" name="launch-offer" id="12-months" value="12"></input>
            <label className="radio-label" for="12-months">$190 USD for 12 months. A $240 USD value. <span className="red-colored">Save $60 USD.</span></label>
          </div>
          <div className="row">
            <input type="radio" name="launch-offer" id="6-months" value="6"></input>
            <label className="radio-label" for="6-months">$150 USD for 6 months. <span className="red-colored">Special one-time offer</span></label>
          </div>
        </div>
      </div>

      <div className="thanktwo_main">
        <form className="form_custom">
                  <div className="form-group">
                  <button className="btn button_jon_sign" style={{float:"left"}}  onClick={()=>{this.props.goBack(3)}} type="button"><img src={require("../../Utils/Assets/aerrow_left.png")} alt="join" /> Go Back</button>
                  <button type="button" className="btn btn-filled pull-right" onClick={this.confirmPayment}>PAY NOW</button>
        </div>
          </form>
        </div>

      </>)
    }

    componentDidMount(){
      $('#page1').addClass('active');
      $('#page2').addClass('active');
      $('#page3').addClass('active');
      $('#page4').addClass('active');
    }
}