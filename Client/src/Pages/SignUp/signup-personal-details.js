import React,{Component} from 'react';
import axios from 'axios';
import  {notify} from 'react-notify-toast';
import $ from 'jquery';

export default class PersonalDetails extends Component{

    constructor(props){
        super(props);
        this.state={
            countries:[],
            display_name:"",
            email:"",
            password:"",
            country_id:0,
            showpwd:false
        }

        this.stepOneSubmit=this.stepOneSubmit.bind(this);
        this.handleCountries=this.handleCountries.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangeUsername = this.handleChangeUsername.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
    }

    stepOneSubmit(e){
        e.preventDefault();
        if(this.state.country_id === 0)
            notify.show("Please select country", "custom", 2000, { background: '#f8d7da', text: "#721c24"});
        else
            this.props.step_one_details(this.state);
    }

    handleCountries(e){
        this.setState({country_id:e.target.value});
    }

    handleChangeEmail(e){
        this.setState({email:e.target.value});
    }
    handleChangePassword(e){
        this.setState({password:e.target.value});
    }
    handleChangeUsername(e){
        this.setState({display_name:e.target.value});
    }

    render(){
        return(
            <form className="form_custom" onSubmit={this.stepOneSubmit}>
                  <div className="lft">
                    <div className="form-group">
                      <input type="email" className="form-control email_s" placeholder="Email Address*" onChange={this.handleChangeEmail} required autoFocus/>
                    </div>
                  </div>
                  
                  <div className="rght">
                    <div className="form-group">
                      <input type="text" className="form-control user_s" placeholder="Select username*" onChange={this.handleChangeUsername} required/>
                    </div>
                  </div>

                  <div className="lft">
                    <div className="form-group">
                      <input type={this.state.showpwd?"text":"password"} className="form-control password_s" placeholder="Password*"  onChange={this.handleChangePassword} pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,20}$" required/>
                      <i className={"fa fa-eye"+ (this.state.showpwd?"-slash":"") +" pull-right show-pwd-dark"} onClick={()=>{this.setState({showpwd:!this.state.showpwd})}}></i>
                    </div>
                  </div>
                  
                  <div className="rght">
                    <div className="form-group">
                       <select value={this.state.country_id} onChange={this.handleCountries} className="form-control">
                            <option value="0">Select country</option>
                           {this.state.countries.length?this.getCountries():""}
                       </select>
                    </div>
            </div>
            <div className="note_s">
              <p>( Minimum 8-20 characters, 1 number, 1 upper case ) *</p>
            </div>
                  <div className="button_btft">
                  <div className="form-group">
                  <button className="btn button_jon_sign" style={{float:"left"}}  onClick={()=>{this.props.goBack(0)}} type="button"><img src={require("../../Utils/Assets/aerrow_left.png")} alt="join" /> Go Back</button>
                  <button className="btn btn-filled" type="submit">SAVE</button>
                  </div> 
                 </div>
                  
             </form>
            
        );
    };

    getCountries(){
       return this.state.countries.map(country=>{
        return(
            <option  key={country.id} value={country.id}>{country.name}</option>
        );
       })
        
    }
    componentDidMount(){
       
        axios.get("/api/v1/country")
        .then(response => {
            this.setState({ countries: response.data.result.countries })
  
        }).catch(error => {
          console.log(error);
        })
    }
}