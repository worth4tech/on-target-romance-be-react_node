import React, { Component } from 'react';
import Logo from '../../Components/left-logo';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { notify } from 'react-notify-toast';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { HashLoader } from 'react-spinners';
import ReactHtmlParser from 'react-html-parser'
import { css } from '@emotion/core';
import { signuppage, register, verifyOTP, resendOTP, paymentConfirmation, verifyPRCode } from '../../Actions/index';
import OTP from './signup-otp';
import PersonalDetails from './signup-personal-details';
import Checkout from './checkout';
import axios from 'axios';
import $ from 'jquery';
import './signup.css'

const override = css`
    position:fixed;
    margin-top: 15%;
    left: 45%;
`;

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      AuthorTypes: [],
      SelectedAuthor: {},
      AuthorTypeInfo: {},
      MembershipDetails: "<li>Details</li>",
      modal: false
    }

    this.toggle = this.toggle.bind(this);
    this.step_one_details = this.step_one_details.bind(this);
    this.verify_otp = this.verify_otp.bind(this);
    this.resend_otp = this.resend_otp.bind(this);
    this.complete_signup = this.complete_signup.bind(this);
    this.previous = this.previous.bind(this);
  }

  handleAuthorChange(author) {
    this.setState({
      SelectedAuthor: author
    });
  }

  toggle(author) {
    this.setState(prevState => ({
      modal: !prevState.modal,
      AuthorTypeInfo: author
    }));
  }

  previous(page) {
    this.props.handleSignUpPages(page);
  }

  step_one_details(details) {
    localStorage.setItem("selected_type", this.state.SelectedAuthor.id);
    this.props.signUp("/api/v1/signup/step/1", details.display_name, details.email, details.password, details.country_id, this.state.SelectedAuthor.id);

  }

  verify_otp(otp) {
    this.props.handleOTPVerification("/api/v1/verify/email", localStorage.getItem("username"), otp);
  }

  resend_otp() {
    this.props.handleResendOTP("/api/v1/resend/otp", localStorage.getItem("username"));
  }

  complete_signup() {
    this.props.completeSignup("/api/v1/pay/membership/");
  }

  validatePRCode() {
    if (document.getElementById("code").value !== "")
      this.props.validatePR("/api/v1/follow/check/validity/" + document.getElementById("code").value);
    else
      notify.show("Please enter a valid PR code.", "custom", 2000, { background: '#f8d7da', text: "#721c24" });
  }

  render() {
    return (
      <React.Fragment>
        <div style={{ backgroundColor: "#1f1f1f" }}>
          <div className="container-fluid">
            <div className="row signupmain">
              <Logo />
              <div className={this.props.page > 0 ? "main_right" : "main_right author_types"}>
                {this.props.page > 0 ? this.SignUpProgress(this.props.page) : ""}
                {this.props.loading ? "" : this.props.page === 1 ?
                  <PersonalDetails step_one_details={this.step_one_details} goBack={this.previous} /> :
                  this.props.page === 2 ?
                    <OTP otp={this.verify_otp} resend={this.resend_otp} /> :
                    this.props.page === 3 ?
                      <PRCode goBack={this.previous} validatePR={this.validatePRCode.bind(this)} /> :
                      this.props.page === 4 ?
                        <Checkout goBack={this.previous}
                            member={this.state.AuthorTypes.length > 0 ?
                            this.state.AuthorTypes.filter((e) => e.id === parseInt(localStorage.getItem("selected_type")))[0].type_name : ""}
                            checkout={this.complete_signup} details={this.state.AuthorTypes.length > 0 ? this.state.AuthorTypes.filter((e) => e.id === parseInt(localStorage.getItem("selected_type")))[0].description : ""} /> :
                        this.props.page === 5 ? <ThankYou /> :
                          this.signUpPage1()
                }
              </div>
            </div>
          </div>
        </div>

        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} id="myModal">
          <div className="model_cover">
            <ModalHeader toggle={this.toggle}> {this.state.AuthorTypeInfo.type_name} Membership
            </ModalHeader>
            <ModalBody>
              {this.state.AuthorTypeInfo ? ReactHtmlParser(this.state.AuthorTypeInfo.description) : <h2>NO DESCRIPTION AVAILABLE</h2>}
            </ModalBody>
          </div>
        </Modal>
      </React.Fragment>
    );
  }



  SignUpProgress(page) {

    return (<div className="headre_progress">
      <ul id="progress-bar" className="progressbar">
        <li id="page1" className={page >= 1 ? "active" : ""}></li>
        <li id="page2" className={page >= 2 ? "active" : ""}></li>
        <li id="page3" className={page >= 3 ? "active" : ""}></li>
        <li id="page4" className={page >= 4 ? "active" : ""}></li>
        <li id="page5" className={page >= 5 ? "active" : ""}></li>
      </ul>
    </div>);
  }

  signUpPage1() {
    return (<React.Fragment>
      <div className="full_title">
        <div className="form-group">
          <div className="in_form">
            <div className="in_title_two"> <span style={{ textTransform: "uppercase" }}>Select Member Type:</span> </div>
            <div className="radiobuttons">
              {this.state.AuthorTypes.length ? this.getAuthorTypes(this.state.AuthorTypes) : ""}
            </div></div></div></div>
      <div className="signuptype_footercover">
        <div className="lft"> </div>
        <div className="right">
          <button type="button" className="btn btn-filled"
            onClick={() => {
              this.state.SelectedAuthor.id ? this.props.handleSignUpPages(1) :
                notify.show("Please select member type.", "custom", 2000, { background: '#f8d7da', text: "#721c24" });
            }} >
            JOIN </button>
        </div>
      </div></React.Fragment>)
  }

  getAuthorTypes(Authors) {
    return Authors.map((author, index) => {
      return (
        // <label key={author.type_name + "_" + author.id} className="" htmlFor="radio1">
        <div key={author.type_name + "_" + author.id} className="rdio rdio-primary radio-inline radioninline_3">
          <input
            name="radio"
            id={index}
            value={author.id}
            type="radio"
            defaultChecked={this.state.SelectedAuthor.type_name === author.type_name}
            onChange={this.handleAuthorChange.bind(this, author)}
            style={{ boxSizing: "border-box" }}
            className="author-radio" />
          <label htmlFor={index}>
            {author.type_name}
          </label>
          <img src={require("../../Utils/Assets/info.png")} alt="info" className="info"
            onClick={() => { this.toggle(author) }} ></img>
        </div>
        // </label>

      );
    })
  }

  componentDidMount() {
    axios.get("/api/v1/user-type")
      .then(response => {
        this.setState({ AuthorTypes: response.data.result.userTypes })

      }).catch(error => {
        console.log(error);
      })
  }

}

const ThankYou = () => {
  localStorage.removeItem("selected_type");
  $('#page1').addClass('active');
  $('#page2').addClass('active');
  $('#page3').addClass('active');
  $('#page4').addClass('active');

  $('#page5').addClass('active');
  return (<div className="thankyou_cover">
    <div className="inner_thankyou">
      <h1>Thank You</h1>
      <p>For Becoming An </p>
      <p>ON target reader Member</p>
      <div className="button_btft" style={{ marginTop: "10%" }}>
        <div className="form-group">
          <Link to="/log-in"><button className="btn btn-filled" type="button">Login</button></Link>
        </div>
      </div>
    </div>
  </div>);
}

const PRCode = (props) => {
  $('#page1').addClass('active');
  $('#page2').addClass('active');
  $('#page3').addClass('active');
  return (<React.Fragment>
    <div className="signup_text">
      <span><strong>If you have an invite code from author, PR firm please add here:</strong></span>
    </div>
    <div className="code_main">
      <div className="form-group form-group_code">
        <input type="text" className="form-control code_form" id="code" placeholder="Enter invite code" />
      </div>
    </div>
    <div className="signup_text note-txt">
      <span>*Upon using special invitation code provided by author, you will be set to automatically follow
         the author to stay informed of all events.<br />To edit or add to your follow list look for following tab under list section.</span>
    </div>

    <div className="signup-btn-group">
      <button className="btn btn-bordered m-r-10 m-t-120" onClick={() => props.goBack(4)} type="button">Skip<img src={require("../../Utils/Assets/aerrow_right.png")} alt="Skip" /></button>
      <button className="btn btn-filled m-t-120" id="Next" onClick={() => props.validatePR()} type="button">Submit</button>
    </div>

  </React.Fragment>);
};

const mapStateToProps = (state) => {
  return {
    page: state.signuppage,
    status: state.userloading,
    loading: state.signuploading
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleSignUpPages: (page) => dispatch(signuppage(page)),
    signUp: (url, name, email, password, country_id, type_id) => dispatch(register(url, name, email, password, country_id, type_id)),
    handleOTPVerification: (url, email, otp) => dispatch(verifyOTP(url, email, otp)),
    handleResendOTP: (url, email) => dispatch(resendOTP(url, email)),
    validatePR: (url) => dispatch(verifyPRCode(url)),
    completeSignup: (url) => dispatch(paymentConfirmation(url))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SignUp);